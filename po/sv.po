# Swedish translation for fragments.
# Copyright © 2019-2025 fragments's COPYRIGHT HOLDER
# This file is distributed under the same license as the fragments package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2019, 2020, 2022, 2023, 2024, 2025.
# Luna Jernberg <droidbittin@gmail.com>, 2022, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: fragments main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Fragments/issues\n"
"POT-Creation-Date: 2025-02-10 14:38+0000\n"
"PO-Revision-Date: 2025-02-10 21:10+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.5\n"

#: data/de.haeckerfelix.Fragments.desktop.in.in:13
msgid "bittorrent;torrent;magnet;download;p2p;"
msgstr "bittorrent;torrent;magnet;nedladdning;hämtning;p2p;"

#. General
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:10
msgid "Manage torrents"
msgstr "Hantera torrentfiler"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:12
msgid ""
"An easy to use BitTorrent client. Fragments can be used to transfer files "
"via the BitTorrent peer-to-peer file-sharing protocol, such as videos, music "
"or installation images for Linux distributions."
msgstr ""
"En lättanvänd BitTorrent-klient. Fragments kan användas för att överföra "
"filer med BitTorrent-protokollet för P2P-fildelning, såsom videoklipp, musik "
"eller installationsavbilder för Linux-distributioner."

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:16
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:65
msgid "Overview of all torrents grouped by status"
msgstr "Översikt över alla torrentfiler, grupperade efter status"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:17
msgid "Schedule the order of downloads with a queue"
msgstr "Schemalägg hämtningars ordning med en kö"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:18
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:73
msgid "Automatic detection of torrent or magnet links from the clipboard"
msgstr ""
"Automatisk identifiering av torrentfiler eller magnetlänkar från urklipp"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:19
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:69
msgid "Manage and access individual files of a torrent"
msgstr "Hantera och kom åt enskilda filer i en torrent"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:20
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:77
msgid "Connect to remote Fragments or Transmission sessions"
msgstr "Anslut till Fragments- eller Transmission-sessioner på distans"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:22
msgid "Fragments uses the Transmission BitTorrent project under the hood."
msgstr ""
"Fragments använder sig under huven av BitTorrent-projektet Transmission."

#. Branding
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:29
msgid "Felix Häcker"
msgstr "Felix Häcker"

#: data/gtk/add_connection_dialog.ui:9
msgid "Add Remote Connection"
msgstr "Lägg till fjärranslutning"

#: data/gtk/add_connection_dialog.ui:19
msgid "Cancel"
msgstr "Avbryt"

#: data/gtk/add_connection_dialog.ui:25
msgid "_Connect"
msgstr "_Anslut"

#: data/gtk/add_connection_dialog.ui:57
msgid ""
"Enter the address of a remote Fragments or Transmission session to connect "
"to."
msgstr ""
"Ange adressen för en fjärran Fragments- eller Transmission-session att "
"ansluta till."

#: data/gtk/add_connection_dialog.ui:60 data/gtk/torrent_row.ui:118
msgid "Name"
msgstr "Namn"

#: data/gtk/add_connection_dialog.ui:67
msgid "Host"
msgstr "Värd"

#: data/gtk/add_connection_dialog.ui:89
msgid "Advanced Settings"
msgstr "Avancerade inställningar"

#: data/gtk/add_connection_dialog.ui:92
msgid "Port"
msgstr "Port"

#: data/gtk/add_connection_dialog.ui:108
msgid "URL Path"
msgstr "URL-sökväg"

#: data/gtk/add_connection_dialog.ui:116
msgid "SSL"
msgstr "SSL"

#: data/gtk/connection_box.ui:8
msgid "Preferences"
msgstr "Inställningar"

#: data/gtk/connection_box.ui:53
msgid "Metered Network"
msgstr "Nätverk med datakvot"

#: data/gtk/connection_box.ui:54
msgid ""
"Torrents have been paused to save data, as metered networks have data limits "
"or charges associated with them."
msgstr ""
"Torrentfiler har pausats för att spara data, då nätverk med datakvoter "
"medför databegränsningar eller kostnader."

#: data/gtk/connection_box.ui:58
msgid "Resume Torrents"
msgstr "Återuppta torrentfiler"

#: data/gtk/connection_box.ui:76
msgid "Get Some Torrents"
msgstr "Skaffa några torrentfiler"

#: data/gtk/connection_box.ui:143
msgid "Click a torrent link on a website"
msgstr "Klicka på en torrentlänk på en webbplats"

#: data/gtk/connection_box.ui:156
msgid "Copy a torrent link to the clipboard"
msgstr "Kopiera en torrentlänk till urklipp"

#: data/gtk/connection_box.ui:169
msgid "Choose a torrent file"
msgstr "Välj en torrentfil"

#: data/gtk/connection_box.ui:182
msgid "Drag and drop a torrent"
msgstr "Dra och släpp en torrent"

#: data/gtk/connection_box.ui:212
msgid "Authentication required"
msgstr "Autentisering krävs"

#: data/gtk/connection_box.ui:227
msgid "Username"
msgstr "Användarnamn"

#: data/gtk/connection_box.ui:232
msgid "Password"
msgstr "Lösenord"

#: data/gtk/connection_box.ui:242
msgid "Authenticate"
msgstr "Autentisera"

#: data/gtk/connection_box.ui:263
msgid "Connection error"
msgstr "Anslutningsfel"

#: data/gtk/connection_box.ui:267
msgid "Try Again"
msgstr "Försök igen"

#: data/gtk/connection_row.ui:61
msgid "Remove connection"
msgstr "Ta bort anslutning"

#: data/gtk/file_page.ui:31 data/gtk/torrent_dialog.ui:143
msgid "Open"
msgstr "Öppna"

#: data/gtk/file_page.ui:44
msgid "Open Containing Folder"
msgstr "Öppna innehållande mapp"

#: data/gtk/file_page.ui:61
msgid "Download"
msgstr "Hämta"

#: data/gtk/file_row.ui:55 data/gtk/status_header.ui:90
msgid "File progress"
msgstr "Filförlopp"

#: data/gtk/folder_page_contents.ui:12
msgid "Sorting"
msgstr "Sortering"

#: data/gtk/folder_page_contents.ui:18 data/gtk/window.ui:43
msgid "Search"
msgstr "Sök"

#: data/gtk/folder_page_contents.ui:29
msgid "Search for files…"
msgstr "Sök filer…"

#: data/gtk/folder_page_contents.ui:36
msgid "Search files"
msgstr "Sök filer"

#: data/gtk/folder_page_contents.ui:102
msgid "Select A_ll"
msgstr "Markera a_lla"

#: data/gtk/folder_page_contents.ui:106
msgid "D_eselect All"
msgstr "A_vmarkera alla"

#: data/gtk/folder_page_contents.ui:112
msgid "_Default"
msgstr "Stan_dard"

#: data/gtk/folder_page_contents.ui:117
msgid "_Name"
msgstr "_Namn"

#: data/gtk/folder_page_contents.ui:122
msgid "_Size"
msgstr "_Storlek"

#: data/gtk/folder_page_contents.ui:129
msgid "_Ascending"
msgstr "S_tigande"

#: data/gtk/folder_page_contents.ui:134
msgid "_Descending"
msgstr "_Fallande"

#: data/gtk/folder_page_contents.ui:141
msgid "_Folders Before Files"
msgstr "_Mappar före filer"

#: data/gtk/preferences_dialog.ui:7
msgid "General"
msgstr "Allmänt"

#: data/gtk/preferences_dialog.ui:10
msgid "Behavior"
msgstr "Beteende"

#: data/gtk/preferences_dialog.ui:13
msgid "Inhibit _Suspend"
msgstr "Förhindra _vänteläge"

#: data/gtk/preferences_dialog.ui:14
msgid ""
"Prevent the computer from suspending while torrents are being downloaded"
msgstr "Förhindra datorn från att gå in i vänteläge när torrentfiler hämtas"

#: data/gtk/preferences_dialog.ui:26
msgid "_Trash Torrent Files"
msgstr "Stoppa torrentfiler i _papperskorgen"

#: data/gtk/preferences_dialog.ui:27
msgid "Automatically moves torrent files to the trash after adding them"
msgstr ""
"Flyttar automatiskt torrentfiler till papperskorgen efter att de lagts till"

#: data/gtk/preferences_dialog.ui:41
msgid "Notifications"
msgstr "Aviseringar"

#: data/gtk/preferences_dialog.ui:44
msgid "_New Torrent Added"
msgstr "_Ny torrent tillagd"

#: data/gtk/preferences_dialog.ui:56
msgid "_Torrent Completely Downloaded"
msgstr "_Torrent fullständigt hämtad"

#: data/gtk/preferences_dialog.ui:70
msgid "Remote Control"
msgstr "Fjärrkontroll"

#: data/gtk/preferences_dialog.ui:73
msgid "Remote Access"
msgstr "Fjärråtkomst"

#: data/gtk/preferences_dialog.ui:75
msgid ""
"Allow other devices on the local network to access the local Fragments "
"session"
msgstr ""
"Tillåt andra enheter på det lokala nätverket att komma åt den lokala "
"sessionen av Fragments"

#: data/gtk/preferences_dialog.ui:86
msgid "Open Web Interface"
msgstr "Öppna webbgränssnitt"

#: data/gtk/preferences_dialog.ui:103
msgctxt "A page title of the preferences dialog"
msgid "Downloading"
msgstr "Hämtning"

#: data/gtk/preferences_dialog.ui:115
msgid "These settings apply to the remote connection"
msgstr "Dessa inställningar tillämpas på fjärranslutningen"

#: data/gtk/preferences_dialog.ui:127
msgid "Location"
msgstr "Plats"

#: data/gtk/preferences_dialog.ui:130
msgid "_Download Directory"
msgstr "_Hämtningskatalog"

#: data/gtk/preferences_dialog.ui:131
msgid "Where to store downloaded torrents"
msgstr "Var hämtade torrenter ska lagras"

#: data/gtk/preferences_dialog.ui:148
msgid "_Incomplete Torrents"
msgstr "_Ofullständiga torrenter"

#: data/gtk/preferences_dialog.ui:149
msgid "Store incomplete torrents in a different directory"
msgstr "Lagra ofullständiga torrenter i en annan katalog"

#: data/gtk/preferences_dialog.ui:154
msgid "_Incomplete Directory"
msgstr "_Ofullständig katalog"

#: data/gtk/preferences_dialog.ui:155
msgid "Where to store incomplete torrents"
msgstr "Var ofullständiga torrenter ska lagras"

#: data/gtk/preferences_dialog.ui:176
msgid "Queue"
msgstr "Kö"

#: data/gtk/preferences_dialog.ui:179
msgid "Download Queue"
msgstr "Hämtningskö"

#: data/gtk/preferences_dialog.ui:180
msgid "You can decide which torrents should be downloaded first"
msgstr "Du kan bestämma vilka torrenter som ska hämtas först"

#: data/gtk/preferences_dialog.ui:185
msgid "Maximum _Active Downloads"
msgstr "Max _antal aktiva hämtningar"

#: data/gtk/preferences_dialog.ui:186
msgid "Number of maximum parallel downloads at the same time"
msgstr "Maximala antalet parallella hämtningar vid ett tillfälle"

#: data/gtk/preferences_dialog.ui:203
msgid "Automatically _Start Torrents"
msgstr "_Starta torrenter automatiskt"

#: data/gtk/preferences_dialog.ui:204
msgid "Automatically start downloading torrents after adding them"
msgstr "Börja automatiskt hämta torrenter efter att de lagts till"

#: data/gtk/preferences_dialog.ui:221
msgid "Network"
msgstr "Nätverk"

#: data/gtk/preferences_dialog.ui:241
msgid "Listening Port"
msgstr "Port att lyssna på"

#: data/gtk/preferences_dialog.ui:242
msgid ""
"This port gets used by peers on the internet to connect to you. It's "
"important that the communication works, otherwise you will have a limited "
"ability to connect to other peers and suffer from slower download speeds."
msgstr ""
"Denna port används av motparter på internet för att ansluta till dig. Det är "
"viktigt att kommunikationen fungerar, annars kommer du ha begränsad förmåga "
"att ansluta till andra motparter och drabbas av långsammare "
"hämtningshastigheter."

#: data/gtk/preferences_dialog.ui:245
msgid "_Port"
msgstr "_Port"

#: data/gtk/preferences_dialog.ui:246
msgid "Used for incoming connections"
msgstr "Används för inkommande anslutningar"

#: data/gtk/preferences_dialog.ui:261
msgid "_Random Port"
msgstr "_Slumpmässig port"

#: data/gtk/preferences_dialog.ui:262
msgid "Select a random port on each session startup, can improve privacy"
msgstr ""
"Välj en slumpmässig port vid uppstarten av varje session, kan förbättra "
"sekretessen"

#: data/gtk/preferences_dialog.ui:274
msgid "Port _Forwarding"
msgstr "_Vidarebefordran av portar"

#: data/gtk/preferences_dialog.ui:275
msgid "Try using UPnP/NAT-PMP for automatic port forwarding on your router"
msgstr ""
"Pröva använda UPnP/NAT-PMP för automatisk vidarebefordran av portar i din "
"router"

#: data/gtk/preferences_dialog.ui:315
msgid "Internet"
msgstr "Internet"

#: data/gtk/preferences_dialog.ui:344
msgid "Router"
msgstr "Router"

#: data/gtk/preferences_dialog.ui:373
msgid "Computer"
msgstr "Dator"

#: data/gtk/preferences_dialog.ui:386
msgid ""
"You can verify your connection by asking <a href=\"https://"
"transmissionbt.com\">transmissionbt.com</a> to test connecting to your "
"computer as peers would."
msgstr ""
"Du kan verifiera din anslutning genom att be <a href=\"https://"
"transmissionbt.com\">transmissionbt.com</a> att testa ansluta till din dator "
"på samma sätt som motparter skulle göra."

#: data/gtk/preferences_dialog.ui:398
msgid "Test"
msgstr "Testa"

#: data/gtk/preferences_dialog.ui:413
msgid "Peer Limits"
msgstr "Motpartsgränser"

#: data/gtk/preferences_dialog.ui:414
msgid ""
"You may have to lower the limits if your router can't keep up with the "
"demands of P2P."
msgstr ""
"Du kan behöva sänka gränserna om din router inte hänger med i P2P-kraven."

#: data/gtk/preferences_dialog.ui:417
msgid "Maximum Peers per _Torrent"
msgstr "Maximalt antal motparter per _torrent"

#: data/gtk/preferences_dialog.ui:432
msgid "Maximum Peers _Overall"
msgstr "Maximalt t_otalt antal motparter"

#: data/gtk/preferences_dialog.ui:449
msgid "Connection Encryption Mode"
msgstr "Krypteringsläge för anslutning"

#: data/gtk/preferences_dialog.ui:450
msgid ""
"<a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">BitTorrent Protocol Encryption</a> can be "
"used to improve privacy. It also can help to bypass ISP filters."
msgstr ""
"<a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">BitTorrent Protocol Encryption</a> kan "
"användas för att förbättra sekretessen. Det kan också hjälpa till att gå "
"runt ISP-filter."

#: data/gtk/preferences_dialog.ui:453
msgid "_Encryption"
msgstr "_Kryptering"

#: data/gtk/preferences_dialog.ui:459
msgid "Force"
msgstr "Tvinga"

#: data/gtk/preferences_dialog.ui:460
msgid "Prefer"
msgstr "Föredra"

#: data/gtk/preferences_dialog.ui:461
msgid "Allow"
msgstr "Tillåt"

#: data/gtk/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allmänt"

#: data/gtk/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Visa kortkommandon"

#: data/gtk/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Inställningar"

#: data/gtk/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Quit"
msgstr "Avsluta"

#: data/gtk/shortcuts.ui:34
msgctxt "shortcut window"
msgid "Torrents"
msgstr "Torrentfiler"

#: data/gtk/shortcuts.ui:37
msgctxt "shortcut window"
msgid "Add New Torrent"
msgstr "Lägg till ny torrent"

#: data/gtk/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Search"
msgstr "Sök"

#: data/gtk/stats_dialog.ui:6
msgid "Statistics"
msgstr "Statistik"

#: data/gtk/stats_dialog.ui:18
msgid "These statistics are from the remote connection"
msgstr "Denna statistik är från fjärranslutningen"

#. Transmission is the name of the torrent daemon. Don't translate it.
#: data/gtk/stats_dialog.ui:32
msgid "Transmission Daemon"
msgstr "Transmission-demon"

#: data/gtk/stats_dialog.ui:33
msgid "Version"
msgstr "Version"

#: data/gtk/stats_dialog.ui:49
msgid "Current Network Speed"
msgstr "Aktuell nätverkshastighet"

#: data/gtk/stats_dialog.ui:52 data/gtk/torrent_dialog.ui:275
msgid "Download Speed"
msgstr "Hämtningshastighet"

#: data/gtk/stats_dialog.ui:66 data/gtk/torrent_dialog.ui:361
msgid "Upload Speed"
msgstr "Sändningshastighet"

#: data/gtk/stats_dialog.ui:82
msgid "Torrent Count"
msgstr "Torrentantal"

#: data/gtk/stats_dialog.ui:85 data/gtk/stats_dialog.ui:211
msgid "Total"
msgstr "Totalt"

#: data/gtk/stats_dialog.ui:98
msgid "Active"
msgstr "Aktiva"

#: data/gtk/stats_dialog.ui:111 src/utils.rs:124
msgid "Paused"
msgstr "Pausad"

#: data/gtk/stats_dialog.ui:124 data/gtk/stats_dialog.ui:168
#: data/gtk/stats_dialog.ui:229 data/gtk/torrent_dialog.ui:300
msgid "Downloaded"
msgstr "Hämtat"

#: data/gtk/stats_dialog.ui:139
msgid "Usage"
msgstr "Användning"

#: data/gtk/stats_dialog.ui:150
msgid "This Session"
msgstr "Denna session"

#: data/gtk/stats_dialog.ui:155 data/gtk/stats_dialog.ui:216
msgid "Time Active"
msgstr "Aktiv tid"

#: data/gtk/stats_dialog.ui:181 data/gtk/stats_dialog.ui:242
#: data/gtk/torrent_dialog.ui:386
msgid "Uploaded"
msgstr "Skickat"

#: data/gtk/stats_dialog.ui:194 data/gtk/stats_dialog.ui:255
msgid "Added Files"
msgstr "Tillagda filer"

#: data/gtk/stats_dialog.ui:268
msgid "Started"
msgstr "Startad"

#: data/gtk/torrent_dialog.ui:8
msgid "Torrent Details"
msgstr "Torrentdetaljer"

#: data/gtk/torrent_dialog.ui:18
msgid "Overview"
msgstr "Översikt"

#: data/gtk/torrent_dialog.ui:87 data/gtk/torrent_row.ui:234
msgid "Pause"
msgstr "Pausad"

#: data/gtk/torrent_dialog.ui:114 data/gtk/torrent_row.ui:251
msgid "Continue"
msgstr "Fortsätt"

#: data/gtk/torrent_dialog.ui:170
msgid "Remove"
msgstr "Ta bort"

#: data/gtk/torrent_dialog.ui:196
msgid "More"
msgstr "Mer"

#: data/gtk/torrent_dialog.ui:215
msgid "A Problem Has Occurred"
msgstr "Ett problem har inträffat"

#: data/gtk/torrent_dialog.ui:289 data/gtk/torrent_dialog.ui:375
msgid "Peers"
msgstr "Motparter"

#: data/gtk/torrent_dialog.ui:407
msgid "Receiving Files Metadata…"
msgstr "Tar emot filmetadata…"

#: data/gtk/torrent_menu.ui:6
msgid "_Pause"
msgstr "_Pausa"

#: data/gtk/torrent_menu.ui:11
msgid "_Continue"
msgstr "_Fortsätt"

#: data/gtk/torrent_menu.ui:18
msgid "Queue Move _Up"
msgstr "Flytta _upp i kö"

#: data/gtk/torrent_menu.ui:22
msgid "Queue Move _Down"
msgstr "Flytta _ner i kö"

#: data/gtk/torrent_menu.ui:28 src/app.rs:536
msgid "_Open"
msgstr "_Öppna"

#: data/gtk/torrent_menu.ui:32 src/actions.rs:388 src/app.rs:591
msgid "_Remove"
msgstr "_Ta bort"

#: data/gtk/torrent_menu.ui:36
msgid "_More"
msgstr "_Mer"

#: data/gtk/torrent_menu.ui:38
msgid "_Copy Magnet Link"
msgstr "_Kopiera magnetlänk"

#: data/gtk/torrent_menu.ui:42
msgid "_Ask Tracker for More Peers"
msgstr "_Fråga bevakare efter fler motparter"

#: data/gtk/torrent_menu.ui:46
msgid "Set _Location…"
msgstr "Ställ _in plats…"

#: data/gtk/torrent_menu.ui:53
msgid "_Properties"
msgstr "_Egenskaper"

#: data/gtk/torrent_page.ui:6
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Downloading"
msgstr "Hämtar"

#: data/gtk/torrent_page.ui:11
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Queued"
msgstr "Köade"

#: data/gtk/torrent_page.ui:16
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Seeding"
msgstr "Distribuerar"

#: data/gtk/torrent_row.ui:25
msgid "Stopped"
msgstr "Stoppad"

#: data/gtk/torrent_row.ui:70
msgid "Downloading"
msgstr "Hämtar"

#: data/gtk/torrent_row.ui:88
msgid "Uploading"
msgstr "Skickar"

#: data/gtk/torrent_row.ui:162
msgid "Torrent progress"
msgstr "Torrentförlopp"

#: data/gtk/torrent_row.ui:189
msgid "Delete"
msgstr "Ta bort"

#: data/gtk/torrent_row.ui:210
msgid "Queue – Move up"
msgstr "Kö – Flytta upp"

#: data/gtk/torrent_row.ui:218
msgid "Queue – Move down"
msgstr "Kö – Flytta ner"

#: data/gtk/window.ui:20
msgid "Add New Torrent"
msgstr "Lägg till ny torrent"

#: data/gtk/window.ui:27
msgid "Main Menu"
msgstr "Huvudmeny"

#: data/gtk/window.ui:35
msgid "Remote Connection Menu"
msgstr "Fjärranslutningsmeny"

#: data/gtk/window.ui:62
msgid "Search torrents…"
msgstr "Sök torrentfiler…"

#: data/gtk/window.ui:71
msgid "Search torrents"
msgstr "Sök torrentfiler"

#: data/gtk/window.ui:87
msgid "_Resume All"
msgstr "Åte_ruppta alla"

#: data/gtk/window.ui:91
msgid "_Pause All"
msgstr "_Pausa alla"

#: data/gtk/window.ui:95
msgid "_Remove Completed Torrents"
msgstr "Ta _bort färdiga torrenter"

#: data/gtk/window.ui:99
msgid "_Statistics…"
msgstr "_Statistik…"

#: data/gtk/window.ui:105
msgid "_Add Remote Connection…"
msgstr "_Lägg till fjärranslutning…"

#: data/gtk/window.ui:111
msgid "_Preferences"
msgstr "_Inställningar"

#: data/gtk/window.ui:115
msgid "_Keyboard Shortcuts"
msgstr "_Tangentbordsgenvägar"

#: data/gtk/window.ui:119
msgid "_About Fragments"
msgstr "_Om Fragments"

#: src/actions.rs:108
msgid "Unable to copy magnet link to clipboard"
msgstr "Kunde inte kopiera magnetlänk till urklipp"

#: src/actions.rs:117
msgid "Copied magnet link to clipboard"
msgstr "Kopierade magnetlänk till urklipp"

#: src/actions.rs:380
msgid "Remove Torrent?"
msgstr "Ta bort torrent?"

#: src/actions.rs:382
msgid ""
"Once removed, continuing the transfer will require the torrent file or "
"magnet link."
msgstr ""
"Då den tagits bort kommer torrentfilen eller magnetlänken att krävas för att "
"fortsätta överföringen."

#: src/actions.rs:386 src/app.rs:589
msgid "_Cancel"
msgstr "A_vbryt"

#. Check button
#: src/actions.rs:392
msgid "Remove downloaded data as well"
msgstr "Ta även bort hämtade data"

#: src/actions.rs:433
msgid "Set Torrent Location"
msgstr "Ställ in torrentplats"

#: src/actions.rs:437 src/utils.rs:165
msgid "_Select"
msgstr "_Välj"

#: src/actions.rs:446
msgid "Move Data?"
msgstr "Flytta data?"

#: src/actions.rs:447
msgid ""
"In order to continue using the torrent, the data must be available at the "
"new location."
msgstr ""
"För att fortsätta använda torrentfilen måste data finnas tillgängliga på den "
"nya platsen."

#: src/actions.rs:450
msgid "_Only Change Location"
msgstr "Ä_ndra bara plats"

#: src/actions.rs:451
msgid "_Move Data to New Location"
msgstr "_Flytta data till ny plats"

#: src/actions.rs:526
msgid "Unable to open file / folder"
msgstr "Kunde inte öppna fil/mapp"

#: src/actions.rs:533
msgid "Could not open file"
msgstr "Kunde inte öppna fil"

#: src/app.rs:401
msgid "Downloading data…"
msgstr "Hämtar data…"

#: src/app.rs:465
msgid "Unsupported file format"
msgstr "Filformatet stöds inte"

#: src/app.rs:535
msgid "Open Torrents"
msgstr "Öppna torrenter"

#: src/app.rs:540
msgid "Torrent files"
msgstr "Torrentfiler"

#: src/app.rs:545
msgid "All files"
msgstr "Alla filer"

#. NOTE: Title of the modal when the user removes all downloaded torrents
#: src/app.rs:580
msgid "Remove all Downloaded Torrents?"
msgstr "Ta bort alla hämtade torrentfiler?"

#: src/app.rs:584
msgid ""
"This will only remove torrents from Fragments, but will keep the downloaded "
"content."
msgstr ""
"Detta kommer endast ta bort torrentfiler från Fragments, men kommer bevara "
"det hämtade innehållet."

#: src/backend/connection.rs:95
msgid "Local Fragments session"
msgstr "Lokal Fragments-session"

#: src/backend/connection_manager.rs:233 src/backend/connection_manager.rs:426
msgid "Unable to stop transmission-daemon"
msgstr "Kunde inte stoppa transmission-daemon"

#: src/backend/connection_manager.rs:249 src/backend/connection_manager.rs:383
msgid "Unable to start transmission-daemon"
msgstr "Kunde inte starta transmission-daemon"

#: src/ui/about_dialog.rs:41
msgid "translator-credits"
msgstr ""
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Luna Jernberg <droidbittin@gmail.com>\n"
"\n"
"Skicka synpunkter på översättningen till\n"
"<tp-sv@listor.tp-sv.se>."

#: src/ui/about_dialog.rs:42
msgid "Donate"
msgstr "Donera"

#: src/ui/add_connection_dialog.rs:113
msgid ""
"Could not connect with “{}”:\n"
"{}"
msgstr ""
"Kunde inte ansluta till ”{}”:\n"
"{}"

#: src/ui/connection_box.rs:106
msgid "The configured download directory cannot be accessed."
msgstr "Kan inte komma åt den konfigurerade hämtningskatalogen."

#: src/ui/connection_box.rs:109
msgid "The configured incomplete directory cannot be accessed."
msgstr ""
"Kan inte komma åt den konfigurerade katalogen för ofullständiga hämtningar."

#: src/ui/connection_box.rs:172
msgid "Connection with {} was lost."
msgstr "Anslutning till {} förlorades."

#: src/ui/connection_box.rs:291
msgid "Credentials could not be saved"
msgstr "Autentiseringsuppgifter kunde inte sparas"

#: src/ui/connection_box.rs:331
msgid "No connection found with UUID \"{}\""
msgstr "Ingen anslutning med UUID ”{}” hittades"

#: src/ui/connection_box.rs:376
msgid "Failed to establish a connection with the Transmission service (“{}”)."
msgstr ""
"Misslyckades med att etablera en anslutning till Transmission-tjänsten "
"(”{}”)."

#: src/ui/connection_row.rs:85
msgid "This computer"
msgstr "Den här datorn"

#: src/ui/file_row.rs:207
msgid "{} / {}, {} item"
msgid_plural "{} / {}, {} items"
msgstr[0] "{} / {}, {} objekt"
msgstr[1] "{} / {}, {} objekt"

#: src/ui/file_row.rs:209
msgid "{} / {}"
msgstr "{} / {}"

#: src/ui/file_row.rs:212
msgid "{}, {} item"
msgid_plural "{}, {} items"
msgstr[0] "{}, {} objekt"
msgstr[1] "{}, {} objekt"

#: src/ui/preferences_dialog.rs:177
msgid "Unable to restart Transmission daemon"
msgstr "Kunde inte starta om Transmission-demon"

#: src/ui/preferences_dialog.rs:356
msgid "Testing…"
msgstr "Testar…"

#: src/ui/preferences_dialog.rs:367
msgid "Port is open. You can communicate with other peers."
msgstr "Porten är öppen. Du kan kommunicera med andra motparter."

#: src/ui/preferences_dialog.rs:370
msgid ""
"Port is closed. Communication with other peers is limited.\n"
"Check your router or firewall if port forwarding is enabled for your "
"computer. "
msgstr ""
"Porten är öppen. Kommunikation med andra motparter är begränsad.\n"
"Kontrollera din router eller brandvägg för att se om vidarebefordran av "
"portar är aktiverad för din dator. "

#: src/ui/preferences_dialog.rs:375
msgid "Test failed. Make sure you are connected to the internet."
msgstr "Test misslyckades. Försäkra dig om att du är ansluten till internet."

#: src/ui/preferences_dialog.rs:406 src/ui/stats_dialog.rs:306
msgid "You are connected with “{}”"
msgstr "Du är ansluten till ”{}”"

#: src/ui/stats_dialog.rs:290
msgid "{} time"
msgid_plural "{} times"
msgstr[0] "{} gång"
msgstr[1] "{} gånger"

#: src/ui/torrent_dialog.rs:371
msgid "Show All"
msgid_plural "Show All {} Items"
msgstr[0] "Visa alla"
msgstr[1] "Visa alla {} objekt"

#: src/ui/torrent_dialog.rs:425
msgid "{} ({} active)"
msgid_plural "{} ({} active)"
msgstr[0] "{} ({} aktiv)"
msgstr[1] "{} ({} aktiva)"

#. Translators: First {} is the amount uploaded, second {} is the
#. uploading speed
#. Displayed under the torrent name in the main window when torrent
#. download is finished
#: src/ui/torrent_row.rs:255
msgid "{} uploaded · {}"
msgstr "{} skickat · {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size
#. Displayed under the torrent name in the main window when torrent
#. is stopped
#: src/ui/torrent_row.rs:263
msgid "{} of {}"
msgstr "{} av {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size, third {} is the download speed
#. Displayed under the torrent name in the main window when torrent
#. is downloading
#: src/ui/torrent_row.rs:269
msgid "{} of {} · {}"
msgstr "{} av {} · {}"

#: src/ui/window.rs:112
msgid "Remote control \"{}\""
msgstr "Fjärrkontroll ”{}”"

#: src/ui/window.rs:207
msgid "New torrent added"
msgstr "Ny torrent tillagd"

#: src/ui/window.rs:221
msgid "Torrent completely downloaded"
msgstr "Torrent fullständigt hämtad"

#: src/ui/window.rs:300
msgid "Add magnet link “{}” from clipboard?"
msgstr "Lägg till magnetlänk ”{}” från urklipp?"

#: src/ui/window.rs:301
msgid "Add magnet link from clipboard?"
msgstr "Lägg till magnetlänk från urklipp?"

#: src/ui/window.rs:306 src/ui/window.rs:327
msgid "_Add"
msgstr "_Lägg till"

#: src/ui/window.rs:315
msgid "Add torrent from “{}”?"
msgstr "Lägg till torrent från ”{}”?"

#: src/ui/window.rs:322
msgid "Add “{}” from clipboard?"
msgstr "Lägg till ”{}” från urklipp?"

#: src/utils.rs:96
msgid "more than a day"
msgstr "mer än en dag"

# https://gitlab.gnome.org/World/Fragments/issues/70
#: src/utils.rs:104
msgid "{} day"
msgid_plural "{} days"
msgstr[0] "{} dag"
msgstr[1] "{} dagar"

#: src/utils.rs:105
msgid "{} hour"
msgid_plural "{} hours"
msgstr[0] "{} timme"
msgstr[1] "{} timmar"

#: src/utils.rs:106
msgid "{} minute"
msgid_plural "{} minutes"
msgstr[0] "{} minut"
msgstr[1] "{} minuter"

#: src/utils.rs:107
msgid "{} second"
msgid_plural "{} seconds"
msgstr[0] "{} sekund"
msgstr[1] "{} sekunder"

#. i18n: "Queued" is the current status of a torrent
#: src/utils.rs:130 src/utils.rs:133 src/utils.rs:136
msgid "Queued"
msgstr "Köad"

#: src/utils.rs:131
msgid "Checking…"
msgstr "Kontrollerar…"

#: src/utils.rs:134
msgid "Seeding…"
msgstr "Distribuerar…"

#: src/utils.rs:158
msgid "Select Download Directory"
msgstr "Välj hämtningskatalog"

#: src/utils.rs:159
msgid "Select Incomplete Directory"
msgstr "Välj ofullständig katalog"

#~ msgid "A BitTorrent Client"
#~ msgstr "En BitTorrent-klient"

#~ msgid "A BitTorrent Client – Based on Transmission Technology"
#~ msgstr "En BitTorrent-klient – Baserad på Transmission-teknologi"

#~ msgid "Appearance"
#~ msgstr "Utseende"

#~ msgid "_Dark Theme"
#~ msgstr "_Mörkt tema"

#~ msgid "Whether Fragments should use a dark theme"
#~ msgstr "Huruvida Fragments ska använda ett mörkt tema"

#~ msgid "Could not show the containing folder"
#~ msgstr "Kunde inte visa innehållande mapp"
