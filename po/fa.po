# Persian translation for fragments.
# Copyright (C) 2023 fragments's COPYRIGHT HOLDER
# This file is distributed under the same license as the fragments package.
# Danial Behzadi <dani.behzi@ubuntu.com>, 2023-2025.
# Reza Hosseinzadeh <rezahosseinzadeh@riseup.net>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: fragments main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Fragments/issues\n"
"POT-Creation-Date: 2025-02-10 14:38+0000\n"
"PO-Revision-Date: 2025-02-11 02:05+0330\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Azerbaijani\n"
"Language: fa_IR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.5\n"

#: data/de.haeckerfelix.Fragments.desktop.in.in:13
msgid "bittorrent;torrent;magnet;download;p2p;"
msgstr "bittorrent;torrent;magnet;download;p2p;تورنت;بیت‌تورنت;مگنت;بارگیری;"

#. General
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:10
msgid "Manage torrents"
msgstr "مدیریت تورنت‌ها"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:12
msgid ""
"An easy to use BitTorrent client. Fragments can be used to transfer files via "
"the BitTorrent peer-to-peer file-sharing protocol, such as videos, music or "
"installation images for Linux distributions."
msgstr ""
"کارخواه بیت‌تورنت ساده. قطعه‌ها می‌تواند برای جابه‌جایی پرونده‌هایی چون ویدیو، آهنگ "
"یا تصویرهای نصب توزیع‌های گنو/لینوکسی با شیوه‌نامهٔ هم‌رسانی نظیر به نظیر بیت‌تورنت "
"استفاده شود."

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:16
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:65
msgid "Overview of all torrents grouped by status"
msgstr "نمای کلی همهٔ تورنت‌ها گروه شده بر پایهٔ وضعیت"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:17
msgid "Schedule the order of downloads with a queue"
msgstr "زمان‌بندی ترتیب بارگیری‌ها با صف"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:18
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:73
msgid "Automatic detection of torrent or magnet links from the clipboard"
msgstr "تشخیص خودکار تورنت یا پیوندهای مغناطیسی از تخته‌گیره"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:19
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:69
msgid "Manage and access individual files of a torrent"
msgstr "مدیریت و دسترسی به پرونده‌های داخل یک تورنت"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:20
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:77
msgid "Connect to remote Fragments or Transmission sessions"
msgstr "وصل شدن به نشست‌های دوردست قطعه‌ها یا ترنسمیشن"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:22
msgid "Fragments uses the Transmission BitTorrent project under the hood."
msgstr "قطعه‌ها در پس‌زمینه از پروژهٔ بیت‌تورنت ترنسمیشن استفاده می‌کند."

#. Branding
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:29
msgid "Felix Häcker"
msgstr "فلیکس هکر"

#: data/gtk/add_connection_dialog.ui:9
msgid "Add Remote Connection"
msgstr "افزودن اتّصال دوردست"

#: data/gtk/add_connection_dialog.ui:19
msgid "Cancel"
msgstr "لغو"

#: data/gtk/add_connection_dialog.ui:25
msgid "_Connect"
msgstr "_وصل شدن"

#: data/gtk/add_connection_dialog.ui:57
msgid ""
"Enter the address of a remote Fragments or Transmission session to connect to."
msgstr "نشانی نشست دوردست قطعه‌ها یا ترنسمیشن را برای وصل شدن وارد کنید."

#: data/gtk/add_connection_dialog.ui:60 data/gtk/torrent_row.ui:118
msgid "Name"
msgstr "نام"

#: data/gtk/add_connection_dialog.ui:67
msgid "Host"
msgstr "میزبان"

#: data/gtk/add_connection_dialog.ui:89
msgid "Advanced Settings"
msgstr "تنظیمات پیش‌رفته"

#: data/gtk/add_connection_dialog.ui:92
msgid "Port"
msgstr "درگاه"

#: data/gtk/add_connection_dialog.ui:108
msgid "URL Path"
msgstr "مسیر نشانی"

#: data/gtk/add_connection_dialog.ui:116
msgid "SSL"
msgstr "SSL"

#: data/gtk/connection_box.ui:8
msgid "Preferences"
msgstr "ترجیحات"

#: data/gtk/connection_box.ui:53
msgid "Metered Network"
msgstr "شبکهٔ اندازه‌گیری شده"

#: data/gtk/connection_box.ui:54
msgid ""
"Torrents have been paused to save data, as metered networks have data limits or "
"charges associated with them."
msgstr ""
"از آن‌جا که شبکه‌های اندازه‌گیری شده هزینه یا محدویت داده دارند،  تورنت‌ها مکث "
"شده‌اند."

#: data/gtk/connection_box.ui:58
msgid "Resume Torrents"
msgstr "از سر گیری تورنت‌ها"

#: data/gtk/connection_box.ui:76
msgid "Get Some Torrents"
msgstr "گرفتن چند تورنت"

#: data/gtk/connection_box.ui:143
msgid "Click a torrent link on a website"
msgstr "کلیک روی پیوند تورنت در یک پایگاه وب"

#: data/gtk/connection_box.ui:156
msgid "Copy a torrent link to the clipboard"
msgstr "رونوشت از پیوند تورنت در تخته‌گیره"

#: data/gtk/connection_box.ui:169
msgid "Choose a torrent file"
msgstr "گزینش پروندهٔ تورنت"

#: data/gtk/connection_box.ui:182
msgid "Drag and drop a torrent"
msgstr "کشیدن و انداختن تورنت"

#: data/gtk/connection_box.ui:212
msgid "Authentication required"
msgstr "نیازمند هویت‌سنجی"

#: data/gtk/connection_box.ui:227
msgid "Username"
msgstr "نام کاربری"

#: data/gtk/connection_box.ui:232
msgid "Password"
msgstr "گذرواژه"

#: data/gtk/connection_box.ui:242
msgid "Authenticate"
msgstr "تأیید هویت"

#: data/gtk/connection_box.ui:263
msgid "Connection error"
msgstr "خطای اتّصال"

#: data/gtk/connection_box.ui:267
msgid "Try Again"
msgstr "تلاش دوباره"

#: data/gtk/connection_row.ui:61
msgid "Remove connection"
msgstr "برداشتن اتّصال"

#: data/gtk/file_page.ui:31 data/gtk/torrent_dialog.ui:143
msgid "Open"
msgstr "گشودن"

#: data/gtk/file_page.ui:44
msgid "Open Containing Folder"
msgstr "گشودن شاخهٔ پرونده"

#: data/gtk/file_page.ui:61
msgid "Download"
msgstr "بارگیری"

#: data/gtk/file_row.ui:55 data/gtk/status_header.ui:90
msgid "File progress"
msgstr "پیشرفت پرونده"

#: data/gtk/folder_page_contents.ui:12
msgid "Sorting"
msgstr "چینش"

#: data/gtk/folder_page_contents.ui:18 data/gtk/window.ui:43
msgid "Search"
msgstr "جست‌وجو"

#: data/gtk/folder_page_contents.ui:29
msgid "Search for files…"
msgstr "جست‌وجوی پرونده‌ها…"

#: data/gtk/folder_page_contents.ui:36
msgid "Search files"
msgstr "جست‌وجوی پرونده‌ها"

#: data/gtk/folder_page_contents.ui:102
msgid "Select A_ll"
msgstr "گزینش _همه"

#: data/gtk/folder_page_contents.ui:106
msgid "D_eselect All"
msgstr "_ناگزینش همه"

#: data/gtk/folder_page_contents.ui:112
msgid "_Default"
msgstr "_پیش‌گزیده"

#: data/gtk/folder_page_contents.ui:117
msgid "_Name"
msgstr "_نام"

#: data/gtk/folder_page_contents.ui:122
msgid "_Size"
msgstr "_اندازه"

#: data/gtk/folder_page_contents.ui:129
msgid "_Ascending"
msgstr "پی_ش‌رونده"

#: data/gtk/folder_page_contents.ui:134
msgid "_Descending"
msgstr "پ_س‌رونده"

#: data/gtk/folder_page_contents.ui:141
msgid "_Folders Before Files"
msgstr "_شاخه‌ها پیش از پرونده‌ها"

#: data/gtk/preferences_dialog.ui:7
msgid "General"
msgstr "عمومی"

#: data/gtk/preferences_dialog.ui:10
msgid "Behavior"
msgstr "رفتار"

#: data/gtk/preferences_dialog.ui:13
msgid "Inhibit _Suspend"
msgstr "جلوگیری از _تعلیق"

#: data/gtk/preferences_dialog.ui:14
msgid "Prevent the computer from suspending while torrents are being downloaded"
msgstr "پیشگیری از تعلیق رایانه هنگام بارگیری تورنت‌ها"

#: data/gtk/preferences_dialog.ui:26
msgid "_Trash Torrent Files"
msgstr "_دور انداختن پرونده‌های تورنت"

#: data/gtk/preferences_dialog.ui:27
msgid "Automatically moves torrent files to the trash after adding them"
msgstr "جابه‌جایی خودکار پرونده‌های تورنت به زباله‌دان پس از افزودنشان"

#: data/gtk/preferences_dialog.ui:41
msgid "Notifications"
msgstr "آگاهی‌ها"

#: data/gtk/preferences_dialog.ui:44
msgid "_New Torrent Added"
msgstr "تورنت _جدید اضافه شد"

#: data/gtk/preferences_dialog.ui:56
msgid "_Torrent Completely Downloaded"
msgstr "تورنت _کامل بارگرفته شد"

#: data/gtk/preferences_dialog.ui:70
msgid "Remote Control"
msgstr "واپایش دوردست"

#: data/gtk/preferences_dialog.ui:73
msgid "Remote Access"
msgstr "دسترسی دوردست"

#: data/gtk/preferences_dialog.ui:75
msgid ""
"Allow other devices on the local network to access the local Fragments session"
msgstr "اجازه به دیگر افزاره‌ها روی شبکهٔ محلی برای دسترسی به نشست قطعه‌های محلی"

#: data/gtk/preferences_dialog.ui:86
msgid "Open Web Interface"
msgstr "گشودن میانای وب"

#: data/gtk/preferences_dialog.ui:103
msgctxt "A page title of the preferences dialog"
msgid "Downloading"
msgstr "بار گرفتن"

#: data/gtk/preferences_dialog.ui:115
msgid "These settings apply to the remote connection"
msgstr "این تنظیمت به اتّصال دوردست اعمال می‌شوند"

#: data/gtk/preferences_dialog.ui:127
msgid "Location"
msgstr "مکان"

#: data/gtk/preferences_dialog.ui:130
msgid "_Download Directory"
msgstr "شاخهٔ _بارگیری"

#: data/gtk/preferences_dialog.ui:131
msgid "Where to store downloaded torrents"
msgstr "محل نگه‌داری تورنت‌های بارگرفته"

#: data/gtk/preferences_dialog.ui:148
msgid "_Incomplete Torrents"
msgstr "تورنت‌ها _ناقص"

#: data/gtk/preferences_dialog.ui:149
msgid "Store incomplete torrents in a different directory"
msgstr "نگه‌داری تورنت‌های ناقص در شاخه‌ای دیگر"

#: data/gtk/preferences_dialog.ui:154
msgid "_Incomplete Directory"
msgstr "شاخهٔ _ناقص"

#: data/gtk/preferences_dialog.ui:155
msgid "Where to store incomplete torrents"
msgstr "محل نگه‌داری تورنت‌های ناقص"

#: data/gtk/preferences_dialog.ui:176
msgid "Queue"
msgstr "صف"

#: data/gtk/preferences_dialog.ui:179
msgid "Download Queue"
msgstr "صف بارگیری"

#: data/gtk/preferences_dialog.ui:180
msgid "You can decide which torrents should be downloaded first"
msgstr "می‌توانید تصمیم بگیری کدام تورنت‌ها نخست بارگیری شوند"

#: data/gtk/preferences_dialog.ui:185
msgid "Maximum _Active Downloads"
msgstr "بیشینهٔ بارگیری‌های _فعّال"

#: data/gtk/preferences_dialog.ui:186
msgid "Number of maximum parallel downloads at the same time"
msgstr "تعداد بیشینهٔ بارگیری‌های موازی در هر زمان"

#: data/gtk/preferences_dialog.ui:203
msgid "Automatically _Start Torrents"
msgstr "_آغاز خودکار تورنت‌ها"

#: data/gtk/preferences_dialog.ui:204
msgid "Automatically start downloading torrents after adding them"
msgstr "آغاز خودکار بارگیری تورنت‌ها پس از افزودنشان"

#: data/gtk/preferences_dialog.ui:221
msgid "Network"
msgstr "شبکه"

#: data/gtk/preferences_dialog.ui:241
msgid "Listening Port"
msgstr "درگاه شنود"

#: data/gtk/preferences_dialog.ui:242
msgid ""
"This port gets used by peers on the internet to connect to you. It's important "
"that the communication works, otherwise you will have a limited ability to "
"connect to other peers and suffer from slower download speeds."
msgstr ""
"این درگاه به دست نظیرهای روی اینترنت برای وصل شدن به شما استفاده می‌شود. این کار "
"برای ارتباط مهم بوده و در غیر این صورت، امکان محدودیت برای وصل شدن به دیگر "
"نظیرها داشته و از سرعت بارگیری کندتری رنج خواهید برد."

#: data/gtk/preferences_dialog.ui:245
msgid "_Port"
msgstr "_درگاه"

#: data/gtk/preferences_dialog.ui:246
msgid "Used for incoming connections"
msgstr "استفادع شده بری اتّصال‌های ورودی"

#: data/gtk/preferences_dialog.ui:261
msgid "_Random Port"
msgstr "_درگاه کاتوره‌ای"

#: data/gtk/preferences_dialog.ui:262
msgid "Select a random port on each session startup, can improve privacy"
msgstr "گزینش درگاهی کاتوره‌ای در آغاز هر نشست. یم‌تواند محرمانگی را بهبود دهد"

#: data/gtk/preferences_dialog.ui:274
msgid "Port _Forwarding"
msgstr "_انتقال درگاه"

#: data/gtk/preferences_dialog.ui:275
msgid "Try using UPnP/NAT-PMP for automatic port forwarding on your router"
msgstr "تلاش برای استفاده از UPnP/NAT-PMP برای انتقال خودکار درگاه روی مسیریابتان"

#: data/gtk/preferences_dialog.ui:315
msgid "Internet"
msgstr "اینترنت"

#: data/gtk/preferences_dialog.ui:344
msgid "Router"
msgstr "مسیریاب"

#: data/gtk/preferences_dialog.ui:373
msgid "Computer"
msgstr "رایانه"

#: data/gtk/preferences_dialog.ui:386
msgid ""
"You can verify your connection by asking <a href=\"https://"
"transmissionbt.com\">transmissionbt.com</a> to test connecting to your computer "
"as peers would."
msgstr ""
"می‌توانید با درخواست از<a href=\"https://"
"transmissionbt.com\">transmissionbt.com</a> برای آزمودن اتّصال به رایانه‌تان "
"همانند یک نظیر، اتّصالتان را بیازمایید."

#: data/gtk/preferences_dialog.ui:398
msgid "Test"
msgstr "آزمایش"

#: data/gtk/preferences_dialog.ui:413
msgid "Peer Limits"
msgstr "محدودیت‌های نظیر"

#: data/gtk/preferences_dialog.ui:414
msgid ""
"You may have to lower the limits if your router can't keep up with the demands "
"of P2P."
msgstr ""
"اگر مسیریابتان نمی‌تواند پابه‌پای درخواست‌های نظیربه‌نظیر بیاید،‌ممکن است لازم باشد "
"محدودیت‌ها را پایین‌تر بیاورید."

#: data/gtk/preferences_dialog.ui:417
msgid "Maximum Peers per _Torrent"
msgstr "بیشینهٔ نظیر برای _تورنت"

#: data/gtk/preferences_dialog.ui:432
msgid "Maximum Peers _Overall"
msgstr "بیشینهٔ نظیر به طور کلی"

#: data/gtk/preferences_dialog.ui:449
msgid "Connection Encryption Mode"
msgstr "حالت رمزنگاری اتّصال"

#: data/gtk/preferences_dialog.ui:450
msgid ""
"<a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">BitTorrent Protocol Encryption</a> can be used "
"to improve privacy. It also can help to bypass ISP filters."
msgstr ""
"برای بهبود محرمانگی می‌توان از <a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">رمزنگاری شیوه‌نامهٔ بیت‌تورنت</a> استفاده کرد. "
"این کار می‌تواند به دور زدن محدودیت‌های اینترنتی نیز کمک کند."

#: data/gtk/preferences_dialog.ui:453
msgid "_Encryption"
msgstr "_رمزنگاری"

#: data/gtk/preferences_dialog.ui:459
msgid "Force"
msgstr "اجبار"

#: data/gtk/preferences_dialog.ui:460
msgid "Prefer"
msgstr "ترجیح"

#: data/gtk/preferences_dialog.ui:461
msgid "Allow"
msgstr "اجازه"

#: data/gtk/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "عمومی"

#: data/gtk/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "نمایش میان‌برها"

#: data/gtk/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Preferences"
msgstr "ترجیحات"

#: data/gtk/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Quit"
msgstr "خروج"

#: data/gtk/shortcuts.ui:34
msgctxt "shortcut window"
msgid "Torrents"
msgstr "تورنت‌ها"

#: data/gtk/shortcuts.ui:37
msgctxt "shortcut window"
msgid "Add New Torrent"
msgstr "افزودن تورنت جدید"

#: data/gtk/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Search"
msgstr "جست‌وجو"

#: data/gtk/stats_dialog.ui:6
msgid "Statistics"
msgstr "آمارها"

#: data/gtk/stats_dialog.ui:18
msgid "These statistics are from the remote connection"
msgstr "این آمارها از اتّصال دوردست است"

#. Transmission is the name of the torrent daemon. Don't translate it.
#: data/gtk/stats_dialog.ui:32
msgid "Transmission Daemon"
msgstr "خدمت ترنسیمیشن"

#: data/gtk/stats_dialog.ui:33
msgid "Version"
msgstr "نگارش"

#: data/gtk/stats_dialog.ui:49
msgid "Current Network Speed"
msgstr "سرعت شبکهٔ کنونی"

#: data/gtk/stats_dialog.ui:52 data/gtk/torrent_dialog.ui:275
msgid "Download Speed"
msgstr "سرعت بارگیری"

#: data/gtk/stats_dialog.ui:66 data/gtk/torrent_dialog.ui:361
msgid "Upload Speed"
msgstr "سرعت بارگذاری"

#: data/gtk/stats_dialog.ui:82
msgid "Torrent Count"
msgstr "شمار تورنت‌ها"

#: data/gtk/stats_dialog.ui:85 data/gtk/stats_dialog.ui:211
msgid "Total"
msgstr "مجموع"

#: data/gtk/stats_dialog.ui:98
msgid "Active"
msgstr "فعّال"

#: data/gtk/stats_dialog.ui:111 src/utils.rs:124
msgid "Paused"
msgstr "مکث شده"

#: data/gtk/stats_dialog.ui:124 data/gtk/stats_dialog.ui:168
#: data/gtk/stats_dialog.ui:229 data/gtk/torrent_dialog.ui:300
msgid "Downloaded"
msgstr "بارگیری شد"

#: data/gtk/stats_dialog.ui:139
msgid "Usage"
msgstr "استفاده"

#: data/gtk/stats_dialog.ui:150
msgid "This Session"
msgstr "این نشست"

#: data/gtk/stats_dialog.ui:155 data/gtk/stats_dialog.ui:216
msgid "Time Active"
msgstr "زمان فعّال"

#: data/gtk/stats_dialog.ui:181 data/gtk/stats_dialog.ui:242
#: data/gtk/torrent_dialog.ui:386
msgid "Uploaded"
msgstr "بارگذاشته"

#: data/gtk/stats_dialog.ui:194 data/gtk/stats_dialog.ui:255
msgid "Added Files"
msgstr "پرونده‌های افزوده"

#: data/gtk/stats_dialog.ui:268
msgid "Started"
msgstr "آغاز شده"

#: data/gtk/torrent_dialog.ui:8
msgid "Torrent Details"
msgstr "جزییات تورنت"

#: data/gtk/torrent_dialog.ui:18
msgid "Overview"
msgstr "نمای کلی"

#: data/gtk/torrent_dialog.ui:87 data/gtk/torrent_row.ui:234
msgid "Pause"
msgstr "مکث"

#: data/gtk/torrent_dialog.ui:114 data/gtk/torrent_row.ui:251
msgid "Continue"
msgstr "ادامه"

#: data/gtk/torrent_dialog.ui:170
msgid "Remove"
msgstr "برداشتن"

#: data/gtk/torrent_dialog.ui:196
msgid "More"
msgstr "بیش‌تر"

#: data/gtk/torrent_dialog.ui:215
msgid "A Problem Has Occurred"
msgstr "مشکلی پیش آمد"

#: data/gtk/torrent_dialog.ui:289 data/gtk/torrent_dialog.ui:375
msgid "Peers"
msgstr "همتاها"

#: data/gtk/torrent_dialog.ui:407
msgid "Receiving Files Metadata…"
msgstr "گرفتن فرادادهٔ پرونده‌ها…"

#: data/gtk/torrent_menu.ui:6
msgid "_Pause"
msgstr "_مکث"

#: data/gtk/torrent_menu.ui:11
msgid "_Continue"
msgstr "_ادامه‌"

#: data/gtk/torrent_menu.ui:18
msgid "Queue Move _Up"
msgstr "_بالا بردن در صف"

#: data/gtk/torrent_menu.ui:22
msgid "Queue Move _Down"
msgstr "_پایین بردن در صف"

#: data/gtk/torrent_menu.ui:28 src/app.rs:536
msgid "_Open"
msgstr "_گشودن"

#: data/gtk/torrent_menu.ui:32 src/actions.rs:388 src/app.rs:591
msgid "_Remove"
msgstr "_برداشتن"

#: data/gtk/torrent_menu.ui:36
msgid "_More"
msgstr "بیش‌_تر"

#: data/gtk/torrent_menu.ui:38
msgid "_Copy Magnet Link"
msgstr "_رونوشت از پیوند مغناطیسی"

#: data/gtk/torrent_menu.ui:42
msgid "_Ask Tracker for More Peers"
msgstr "_درخواست از ردیاب برای نظیرهای بیش‌تر"

#: data/gtk/torrent_menu.ui:46
msgid "Set _Location…"
msgstr "تنظیم _مکان…"

#: data/gtk/torrent_menu.ui:53
msgid "_Properties"
msgstr "_ویژگی‌ها"

#: data/gtk/torrent_page.ui:6
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Downloading"
msgstr "بار گرفتن"

#: data/gtk/torrent_page.ui:11
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Queued"
msgstr "صف شده"

#: data/gtk/torrent_page.ui:16
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Seeding"
msgstr "دانه دادن"

#: data/gtk/torrent_row.ui:25
msgid "Stopped"
msgstr "متوقّف"

#: data/gtk/torrent_row.ui:70
msgid "Downloading"
msgstr "بار گرفتن"

#: data/gtk/torrent_row.ui:88
msgid "Uploading"
msgstr "بارگذاشتن"

#: data/gtk/torrent_row.ui:162
msgid "Torrent progress"
msgstr "پیشرفت تورنت"

#: data/gtk/torrent_row.ui:189
msgid "Delete"
msgstr "حذف"

#: data/gtk/torrent_row.ui:210
msgid "Queue – Move up"
msgstr "صف – بالا بردن"

#: data/gtk/torrent_row.ui:218
msgid "Queue – Move down"
msgstr "صف – پایین بردن"

#: data/gtk/window.ui:20
msgid "Add New Torrent"
msgstr "افزودن تورنت جدید"

#: data/gtk/window.ui:27
msgid "Main Menu"
msgstr "فهرست اصلی"

#: data/gtk/window.ui:35
msgid "Remote Connection Menu"
msgstr "فهرست اتّصال دوردست"

#: data/gtk/window.ui:62
msgid "Search torrents…"
msgstr "جست‌وجوی تورنت‌ها…"

#: data/gtk/window.ui:71
msgid "Search torrents"
msgstr "جست‌وجوی تورنت‌ها"

#: data/gtk/window.ui:87
msgid "_Resume All"
msgstr "_از سر گیری همه"

#: data/gtk/window.ui:91
msgid "_Pause All"
msgstr "_مکث همه"

#: data/gtk/window.ui:95
msgid "_Remove Completed Torrents"
msgstr "_برداشتن تورنت‌های کامل"

#: data/gtk/window.ui:99
msgid "_Statistics…"
msgstr "_آمارها…"

#: data/gtk/window.ui:105
msgid "_Add Remote Connection…"
msgstr "_افزودن اتّصال دوردست…"

#: data/gtk/window.ui:111
msgid "_Preferences"
msgstr "_ترجیحات"

#: data/gtk/window.ui:115
msgid "_Keyboard Shortcuts"
msgstr "_میان‌برهای صفحه‌کلید"

#: data/gtk/window.ui:119
msgid "_About Fragments"
msgstr "_دربارهٔ قطعه‌ها"

#: src/actions.rs:108
msgid "Unable to copy magnet link to clipboard"
msgstr "ناتوان در رونوشت پیوند مغناطیسی در تخته‌گیره"

#: src/actions.rs:117
msgid "Copied magnet link to clipboard"
msgstr "پیوند مغناطیسی در تخته‌گیره رونوشت شد"

#: src/actions.rs:380
msgid "Remove Torrent?"
msgstr "برداشتن تورنت؟"

#: src/actions.rs:382
msgid ""
"Once removed, continuing the transfer will require the torrent file or magnet "
"link."
msgstr ""
"پس از برداشتن، ادامهٔ انتقال نیازمند پروندهٔ تورنت یا پیوند مغناطیسی خواهد بود."

#: src/actions.rs:386 src/app.rs:589
msgid "_Cancel"
msgstr "_لغو"

#. Check button
#: src/actions.rs:392
msgid "Remove downloaded data as well"
msgstr "همچنین برداشتن داده‌های بارگرفته"

#: src/actions.rs:433
msgid "Set Torrent Location"
msgstr "تنظیم مکان تورنت"

#: src/actions.rs:437 src/utils.rs:165
msgid "_Select"
msgstr "_گزینش"

#: src/actions.rs:446
msgid "Move Data?"
msgstr "جابه‌جایی داده‌ها؟"

#: src/actions.rs:447
msgid ""
"In order to continue using the torrent, the data must be available at the new "
"location."
msgstr "برای ادامهٔ استفاده از تورنت، داده‌ها باید در مکان جدید موجود باشند."

#: src/actions.rs:450
msgid "_Only Change Location"
msgstr "_تنها تغییر مگان"

#: src/actions.rs:451
msgid "_Move Data to New Location"
msgstr "_جابه‌جایی داده‌ها به مکان جدید"

#: src/actions.rs:526
msgid "Unable to open file / folder"
msgstr "ناتوان در گشودن پرونده یا شاخه"

#: src/actions.rs:533
msgid "Could not open file"
msgstr "نتوانست پرونده را بگشاید"

#: src/app.rs:401
msgid "Downloading data…"
msgstr "بار گرفتن داده‌ها…"

#: src/app.rs:465
msgid "Unsupported file format"
msgstr "قالب پروندهٔ پشتیبانی نشده"

#: src/app.rs:535
msgid "Open Torrents"
msgstr "گشودن تورنت‌ها"

#: src/app.rs:540
msgid "Torrent files"
msgstr "پرونده‌های تورنت"

#: src/app.rs:545
msgid "All files"
msgstr "همهٔ پرونده‌ها"

#. NOTE: Title of the modal when the user removes all downloaded torrents
#: src/app.rs:580
msgid "Remove all Downloaded Torrents?"
msgstr "بردشتن تمامی تورنت‌های بارگرفته؟"

#: src/app.rs:584
msgid ""
"This will only remove torrents from Fragments, but will keep the downloaded "
"content."
msgstr ""
"این کار تنها تورنت‌ها را از قطعه‌ها برخواهد داشت. محتواهای بارگرفته همچنان نگه "
"داشته خواهند شد."

#: src/backend/connection.rs:95
msgid "Local Fragments session"
msgstr "نشست قطعه‌های محلی"

#: src/backend/connection_manager.rs:233 src/backend/connection_manager.rs:426
msgid "Unable to stop transmission-daemon"
msgstr "ناتوان در توقّف خدمت ترنسیمیشن"

#: src/backend/connection_manager.rs:249 src/backend/connection_manager.rs:383
msgid "Unable to start transmission-daemon"
msgstr "ناتوان در آغاز خدمت ترنسیمیشن"

#: src/ui/about_dialog.rs:41
msgid "translator-credits"
msgstr "دانیال بهزادی <dani.behzi@ubuntu.com>"

#: src/ui/about_dialog.rs:42
msgid "Donate"
msgstr "اعانه"

#: src/ui/add_connection_dialog.rs:113
msgid ""
"Could not connect with “{}”:\n"
"{}"
msgstr ""
"نتوانست به {} وصل شود:\n"
"{}"

#: src/ui/connection_box.rs:106
msgid "The configured download directory cannot be accessed."
msgstr "شاخهٔ بارگیری پیکربندی شده قابل دسترسی نیست."

#: src/ui/connection_box.rs:109
msgid "The configured incomplete directory cannot be accessed."
msgstr "شاخهٔ ناقص پیکربندی شده قابل دسترسی نیست."

#: src/ui/connection_box.rs:172
msgid "Connection with {} was lost."
msgstr "اتّصال به {} از دست رفت."

#: src/ui/connection_box.rs:291
msgid "Credentials could not be saved"
msgstr "گواهی‌ها نتوانستند ذخیره شوند"

#: src/ui/connection_box.rs:331
msgid "No connection found with UUID \"{}\""
msgstr "اتّصالی با شناسهٔ یکتای {} پیدا نشد"

#: src/ui/connection_box.rs:376
msgid "Failed to establish a connection with the Transmission service (“{}”)."
msgstr "شکست در برقراری ارتباط با خدکت ترنسمیشن ({})."

#: src/ui/connection_row.rs:85
msgid "This computer"
msgstr "این رایانه"

#: src/ui/file_row.rs:207
msgid "{} / {}, {} item"
msgid_plural "{} / {}, {} items"
msgstr[0] "{} / {}، {} مورد"
msgstr[1] "{} / {}، {} مورد"

#: src/ui/file_row.rs:209
msgid "{} / {}"
msgstr "{} / {}"

#: src/ui/file_row.rs:212
msgid "{}, {} item"
msgid_plural "{}, {} items"
msgstr[0] "{}، {} مورد"
msgstr[1] "{}، {} مورد"

#: src/ui/preferences_dialog.rs:177
msgid "Unable to restart Transmission daemon"
msgstr "ناتوان در آفاز دوبارهٔ خدمت ترنسیمیشن"

#: src/ui/preferences_dialog.rs:356
msgid "Testing…"
msgstr "آزمودن…"

#: src/ui/preferences_dialog.rs:367
msgid "Port is open. You can communicate with other peers."
msgstr "درگاه باز است. می‌توانید با دیگر نظیرها ارتباط برقرار کنید."

#: src/ui/preferences_dialog.rs:370
msgid ""
"Port is closed. Communication with other peers is limited.\n"
"Check your router or firewall if port forwarding is enabled for your computer. "
msgstr ""
"درگاه بسته است. ارتباط با دیگر نظیرها محدود است.\n"
"مسیرساب یا دیوار آتشتان را برای به کار افتاده بودن انتقال درگاه به رایانه‌تان "
"بررسی کنید. "

#: src/ui/preferences_dialog.rs:375
msgid "Test failed. Make sure you are connected to the internet."
msgstr "آزمون شکست خورد. مطمئن شوید به اینترنت وصلید."

#: src/ui/preferences_dialog.rs:406 src/ui/stats_dialog.rs:306
msgid "You are connected with “{}”"
msgstr "به {} وصلید"

#: src/ui/stats_dialog.rs:290
msgid "{} time"
msgid_plural "{} times"
msgstr[0] "{} بار"
msgstr[1] "{} بار"

#: src/ui/torrent_dialog.rs:371
msgid "Show All"
msgid_plural "Show All {} Items"
msgstr[0] "نمایش همه"
msgstr[1] "نمایش هر {} بار"

#: src/ui/torrent_dialog.rs:425
msgid "{} ({} active)"
msgid_plural "{} ({} active)"
msgstr[0] "{} ({} فعّال)"
msgstr[1] "{} ({} فعّال)"

#. Translators: First {} is the amount uploaded, second {} is the
#. uploading speed
#. Displayed under the torrent name in the main window when torrent
#. download is finished
#: src/ui/torrent_row.rs:255
msgid "{} uploaded · {}"
msgstr "{} بارگذاشته · {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size
#. Displayed under the torrent name in the main window when torrent
#. is stopped
#: src/ui/torrent_row.rs:263
msgid "{} of {}"
msgstr "{} از {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size, third {} is the download speed
#. Displayed under the torrent name in the main window when torrent
#. is downloading
#: src/ui/torrent_row.rs:269
msgid "{} of {} · {}"
msgstr "{} از {} · {}"

#: src/ui/window.rs:112
msgid "Remote control \"{}\""
msgstr "واپایش دوردست {}"

#: src/ui/window.rs:207
msgid "New torrent added"
msgstr "تورنت جدید اضافه شد"

#: src/ui/window.rs:221
msgid "Torrent completely downloaded"
msgstr "تورنت کامل بارگرفته شد"

#: src/ui/window.rs:300
msgid "Add magnet link “{}” from clipboard?"
msgstr "افزودن پیوند مغناطیسی {} از تخته‌گیره؟"

#: src/ui/window.rs:301
msgid "Add magnet link from clipboard?"
msgstr "افزودن پیوند مغناطیسی از تخته‌گیره؟"

#: src/ui/window.rs:306 src/ui/window.rs:327
msgid "_Add"
msgstr "_افزودن"

#: src/ui/window.rs:315
msgid "Add torrent from “{}”?"
msgstr "افزودن تورنت از «{}»؟"

#: src/ui/window.rs:322
msgid "Add “{}” from clipboard?"
msgstr "افزودن {} از تخته‌گیره؟"

#: src/utils.rs:96
msgid "more than a day"
msgstr "بیش از یک روز"

#: src/utils.rs:104
msgid "{} day"
msgid_plural "{} days"
msgstr[0] "{} روز"
msgstr[1] "{} روز"

#: src/utils.rs:105
msgid "{} hour"
msgid_plural "{} hours"
msgstr[0] "۱ ساعت"
msgstr[1] "{} ساعت"

#: src/utils.rs:106
msgid "{} minute"
msgid_plural "{} minutes"
msgstr[0] "{} دقیقه"
msgstr[1] "{} دقیقه"

#: src/utils.rs:107
msgid "{} second"
msgid_plural "{} seconds"
msgstr[0] "{} ثانیه"
msgstr[1] "{} ثانیه"

#. i18n: "Queued" is the current status of a torrent
#: src/utils.rs:130 src/utils.rs:133 src/utils.rs:136
msgid "Queued"
msgstr "صف شده"

#: src/utils.rs:131
msgid "Checking…"
msgstr "در حال بررسی…"

#: src/utils.rs:134
msgid "Seeding…"
msgstr "دانه دادن…"

#: src/utils.rs:158
msgid "Select Download Directory"
msgstr "گزینش شاخهٔ بارگیری"

#: src/utils.rs:159
msgid "Select Incomplete Directory"
msgstr "گزینش شاخهٔ ناقص"

#~ msgid "@NAME@"
#~ msgstr "قطعه‌ها"

#~ msgid "A BitTorrent Client"
#~ msgstr "کارخواه تورنت"

#~ msgid "A BitTorrent Client – Based on Transmission Technology"
#~ msgstr "کارخوه تورنت – بر پایهٔ فناوری ترنسمیشن"

#~ msgid "Appearance"
#~ msgstr "ظاهر"

#~ msgid "_Dark Theme"
#~ msgstr "زمینهٔ _تیره"

#~ msgid "Whether Fragments should use a dark theme"
#~ msgstr "این که قطعه‌ها باید از زمینه‌ای تیره استفاده کند یا نه"
