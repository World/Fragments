# British English translation for fragments.
# Copyright (C) 2019 fragments's COPYRIGHT HOLDER
# This file is distributed under the same license as the fragments package.
# Zander Brown <zbrown@gnome.org>, 2019.
# Andi Chandler <andi@gowling.com>, 2024.
# Bruce Cowan <bruce@bcowan.me.uk>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: fragments master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Fragments/issues\n"
"POT-Creation-Date: 2024-03-24 19:48+0000\n"
"PO-Revision-Date: 2024-03-26 11:51+0000\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: British English <en_GB@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.2\n"

#: data/de.haeckerfelix.Fragments.desktop.in.in:13
msgid "bittorrent;torrent;magnet;download;p2p;"
msgstr "bittorrent;torrent;magnet;download;p2p;"

#. General
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:10
#| msgid "Search torrents"
msgid "Manage torrents"
msgstr "Manage torrents"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:12
#| msgid ""
#| "Fragments is an easy to use BitTorrent client. It can be used to transfer "
#| "files via the BitTorrent protocol, such as videos, music or installation "
#| "images for Linux distributions."
msgid ""
"An easy to use BitTorrent client. Fragments can be used to transfer files "
"via the BitTorrent peer-to-peer file-sharing protocol, such as videos, music "
"or installation images for Linux distributions."
msgstr ""
"An easy to use BitTorrent client. Fragments can be used to transfer files "
"via the BitTorrent peer-to-peer file-sharing protocol, such as videos, music "
"or installation images for Linux distributions."

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:16
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:64
msgid "Overview of all torrents grouped by status"
msgstr "Overview of all torrents grouped by status"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:17
msgid "Schedule the order of downloads with a queue"
msgstr "Schedule the order of downloads with a queue"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:18
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:72
#| msgid "Add magnet link from clipboard?"
msgid "Automatic detection of torrent or magnet links from the clipboard"
msgstr "Automatic detection of torrent or magnet links from the clipboard"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:19
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:68
msgid "Manage and access individual files of a torrent"
msgstr "Manage and access individual files of a torrent"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:20
#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:76
#| msgid ""
#| "Enter the address of a remote Fragments or Transmission session to "
#| "connect to."
msgid "Connect to remote Fragments or Transmission sessions"
msgstr "Connect to remote Fragments or Transmission sessions"

#: data/de.haeckerfelix.Fragments.metainfo.xml.in.in:22
msgid "Fragments uses the Transmission BitTorrent project under the hood."
msgstr "Fragments uses the Transmission BitTorrent project under the hood."

#: data/gtk/add_connection_dialog.ui:8
msgid "Add Remote Connection"
msgstr "Add Remote Connection"

#: data/gtk/add_connection_dialog.ui:18
msgid "Cancel"
msgstr "Cancel"

#: data/gtk/add_connection_dialog.ui:24
msgid "_Connect"
msgstr "_Connect"

#: data/gtk/add_connection_dialog.ui:56
msgid ""
"Enter the address of a remote Fragments or Transmission session to connect "
"to."
msgstr ""
"Enter the address of a remote Fragments or Transmission session to connect "
"to."

#: data/gtk/add_connection_dialog.ui:59 data/gtk/torrent_row.ui:120
msgid "Name"
msgstr "Name"

#: data/gtk/add_connection_dialog.ui:65
msgid "Host"
msgstr "Host"

#: data/gtk/add_connection_dialog.ui:86
msgid "Advanced Settings"
msgstr "Advanced Settings"

#: data/gtk/add_connection_dialog.ui:89
msgid "Port"
msgstr "Port"

#: data/gtk/add_connection_dialog.ui:105
msgid "URL Path"
msgstr "URL Path"

#: data/gtk/add_connection_dialog.ui:112
msgid "SSL"
msgstr "SSL"

#: data/gtk/connection_box.ui:8
msgid "Preferences"
msgstr "Preferences"

#: data/gtk/connection_box.ui:56
msgid "Metered Network"
msgstr "Metered Network"

#: data/gtk/connection_box.ui:57
msgid ""
"Torrents have been paused to save data, as metered networks have data limits "
"or charges associated with them."
msgstr ""
"Torrents have been paused to save data, as metered networks have data limits "
"or charges associated with them."

#: data/gtk/connection_box.ui:61
msgid "Resume Torrents"
msgstr "Resume Torrents"

#: data/gtk/connection_box.ui:79
msgid "Get Some Torrents"
msgstr "Get Some Torrents"

#: data/gtk/connection_box.ui:145
#| msgid "Click a magnet link on a website"
msgid "Click a torrent link on a website"
msgstr "Click a torrent link on a website"

#: data/gtk/connection_box.ui:155
#| msgid "Copy a magnet link to the clipboard"
msgid "Copy a torrent link to the clipboard"
msgstr "Copy a torrent link to the clipboard"

#: data/gtk/connection_box.ui:165
msgid "Choose a torrent file"
msgstr "Choose a torrent file"

#: data/gtk/connection_box.ui:175
msgid "Drag and drop a torrent"
msgstr "Drag and drop a torrent"

#: data/gtk/connection_box.ui:202
msgid "Authentication required"
msgstr "Authentication required"

#: data/gtk/connection_box.ui:217
msgid "Username"
msgstr "Username"

#: data/gtk/connection_box.ui:222
msgid "Password"
msgstr "Password"

#: data/gtk/connection_box.ui:232
msgid "Authenticate"
msgstr "Authenticate"

#: data/gtk/connection_box.ui:253
msgid "Connection error"
msgstr "Connection error"

#: data/gtk/connection_box.ui:257
msgid "Try Again"
msgstr "Try Again"

#: data/gtk/connection_row.ui:61
msgid "Remove connection"
msgstr "Remove connection"

#: data/gtk/file_page.ui:31 data/gtk/torrent_dialog.ui:143
msgid "Open"
msgstr "Open"

#: data/gtk/file_page.ui:44
msgid "Open Containing Folder"
msgstr "Open Containing Folder"

#: data/gtk/file_page.ui:61
msgid "Download"
msgstr "Download"

#: data/gtk/file_row.ui:55 data/gtk/status_header.ui:90
msgid "File progress"
msgstr "File progress"

#: data/gtk/folder_page_contents.ui:12
msgid "Sorting"
msgstr "Sorting"

#: data/gtk/folder_page_contents.ui:18 data/gtk/window.ui:43
msgid "Search"
msgstr "Search"

#: data/gtk/folder_page_contents.ui:29
msgid "Search for files…"
msgstr "Search for files…"

#: data/gtk/folder_page_contents.ui:36
msgid "Search files"
msgstr "Search files"

#: data/gtk/folder_page_contents.ui:102
msgid "Select A_ll"
msgstr "Select A_ll"

#: data/gtk/folder_page_contents.ui:106
msgid "D_eselect All"
msgstr "D_eselect All"

#: data/gtk/folder_page_contents.ui:112
msgid "_Default"
msgstr "_Default"

#: data/gtk/folder_page_contents.ui:117
msgid "_Name"
msgstr "_Name"

#: data/gtk/folder_page_contents.ui:122
msgid "_Size"
msgstr "_Size"

#: data/gtk/folder_page_contents.ui:129
msgid "_Ascending"
msgstr "_Ascending"

#: data/gtk/folder_page_contents.ui:134
msgid "_Descending"
msgstr "_Descending"

#: data/gtk/folder_page_contents.ui:141
msgid "_Folders Before Files"
msgstr "_Folders Before Files"

#: data/gtk/preferences_dialog.ui:7
msgid "General"
msgstr "General"

#: data/gtk/preferences_dialog.ui:10
msgid "Behavior"
msgstr "Behaviour"

#: data/gtk/preferences_dialog.ui:13
msgid "Inhibit _Suspend"
msgstr "Inhibit _Suspend"

#: data/gtk/preferences_dialog.ui:14
msgid ""
"Prevent the computer from suspending while torrents are being downloaded"
msgstr ""
"Prevent the computer from suspending while torrents are being downloaded"

#: data/gtk/preferences_dialog.ui:26
msgid "_Trash Torrent Files"
msgstr "Move Torrent Files to the Was_tebasket"

#: data/gtk/preferences_dialog.ui:27
msgid "Automatically moves torrent files to the trash after adding them"
msgstr "Automatically moves torrent files to the wastebasket after adding them"

#: data/gtk/preferences_dialog.ui:41
msgid "Notifications"
msgstr "Notifications"

#: data/gtk/preferences_dialog.ui:44
msgid "_New Torrent Added"
msgstr "_New Torrent Added"

#: data/gtk/preferences_dialog.ui:56
msgid "_Torrent Completely Downloaded"
msgstr "_Torrent Completely Downloaded"

#: data/gtk/preferences_dialog.ui:70
msgid "Remote Control"
msgstr "Remote Control"

#: data/gtk/preferences_dialog.ui:73
msgid "Remote Access"
msgstr "Remote Access"

#: data/gtk/preferences_dialog.ui:75
msgid ""
"Allow other devices on the local network to access the local Fragments "
"session"
msgstr ""
"Allow other devices on the local network to access the local Fragments "
"session"

#: data/gtk/preferences_dialog.ui:86
msgid "Open Webinterface"
msgstr "Open Webinterface"

#: data/gtk/preferences_dialog.ui:103
msgctxt "A page title of the preferences dialog"
msgid "Downloading"
msgstr "Downloading"

#: data/gtk/preferences_dialog.ui:115
msgid "These settings apply to the remote connection"
msgstr "These settings apply to the remote connection"

#: data/gtk/preferences_dialog.ui:127
msgid "Location"
msgstr "Location"

#: data/gtk/preferences_dialog.ui:130
msgid "_Download Directory"
msgstr "_Download Directory"

#: data/gtk/preferences_dialog.ui:131
msgid "Where to store downloaded torrents"
msgstr "Where to store downloaded torrents"

#: data/gtk/preferences_dialog.ui:148
msgid "_Incomplete Torrents"
msgstr "_Incomplete Torrents"

#: data/gtk/preferences_dialog.ui:149
msgid "Store incomplete torrents in a different directory"
msgstr "Store incomplete torrents in a different directory"

#: data/gtk/preferences_dialog.ui:154
msgid "_Incomplete Directory"
msgstr "_Incomplete Directory"

#: data/gtk/preferences_dialog.ui:155
msgid "Where to store incomplete torrents"
msgstr "Where to store incomplete torrents"

#: data/gtk/preferences_dialog.ui:176
msgid "Queue"
msgstr "Queue"

#: data/gtk/preferences_dialog.ui:179
msgid "Download Queue"
msgstr "Download Queue"

#: data/gtk/preferences_dialog.ui:180
msgid "You can decide which torrents should be downloaded first"
msgstr "You can decide which torrents should be downloaded first"

#: data/gtk/preferences_dialog.ui:185
msgid "Maximum _Active Downloads"
msgstr "Maximum _Active Downloads"

#: data/gtk/preferences_dialog.ui:186
msgid "Number of maximum parallel downloads at the same time"
msgstr "Number of maximum parallel downloads at the same time"

#: data/gtk/preferences_dialog.ui:203
msgid "Automatically _Start Torrents"
msgstr "Automatically _Start Torrents"

#: data/gtk/preferences_dialog.ui:204
msgid "Automatically start downloading torrents after adding them"
msgstr "Automatically start downloading torrents after adding them"

#: data/gtk/preferences_dialog.ui:221
msgid "Network"
msgstr "Network"

#: data/gtk/preferences_dialog.ui:241
msgid "Listening Port"
msgstr "Listening Port"

#: data/gtk/preferences_dialog.ui:242
msgid ""
"This port gets used by peers on the internet to connect to you. It's "
"important that the communication works, otherwise you will have a limited "
"ability to connect to other peers and suffer from slower download speeds."
msgstr ""
"This port gets used by peers on the Internet to connect to you. It's "
"important that the communication works, otherwise you will have a limited "
"ability to connect to other peers and suffer from slower download speeds."

#: data/gtk/preferences_dialog.ui:245
msgid "_Port"
msgstr "_Port"

#: data/gtk/preferences_dialog.ui:246
msgid "Used for incoming connections"
msgstr "Used for incoming connections"

#: data/gtk/preferences_dialog.ui:261
msgid "_Random Port"
msgstr "_Random Port"

#: data/gtk/preferences_dialog.ui:262
msgid "Select a random port on each session startup, can improve privacy"
msgstr "Select a random port on each session startup, can improve privacy"

#: data/gtk/preferences_dialog.ui:274
msgid "Port _Forwarding"
msgstr "Port _Forwarding"

#: data/gtk/preferences_dialog.ui:275
msgid "Try using UPnP/NAT-PMP for automatic port forwarding on your router"
msgstr "Try using UPnP/NAT-PMP for automatic port forwarding on your router"

#: data/gtk/preferences_dialog.ui:315
msgid "Internet"
msgstr "Internet"

#: data/gtk/preferences_dialog.ui:344
msgid "Router"
msgstr "Router"

#: data/gtk/preferences_dialog.ui:373
msgid "Computer"
msgstr "Computer"

#: data/gtk/preferences_dialog.ui:386
msgid ""
"You can verify your connection by asking <a href=\"https://transmissionbt."
"com\">transmissionbt.com</a> to test connecting to your computer as peers "
"would."
msgstr ""
"You can verify your connection by asking <a href=\"https://transmissionbt."
"com\">transmissionbt.com</a> to test connecting to your computer as peers "
"would."

#: data/gtk/preferences_dialog.ui:398
msgid "Test"
msgstr "Test"

#: data/gtk/preferences_dialog.ui:413
msgid "Peer Limits"
msgstr "Peer Limits"

#: data/gtk/preferences_dialog.ui:414
msgid ""
"You may have to lower the limits if your router can't keep up with the "
"demands of P2P."
msgstr ""
"You may have to lower the limits if your router can't keep up with the "
"demands of P2P."

#: data/gtk/preferences_dialog.ui:417
msgid "Maximum Peers per _Torrent"
msgstr "Maximum Peers per _Torrent"

#: data/gtk/preferences_dialog.ui:432
msgid "Maximum Peers _Overall"
msgstr "Maximum Peers _Overall"

#: data/gtk/preferences_dialog.ui:449
msgid "Connection Encryption Mode"
msgstr "Connection Encryption Mode"

#: data/gtk/preferences_dialog.ui:450
msgid ""
"<a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">BitTorrent Protocol Encryption</a> can be "
"used to improve privacy. It also can help to bypass ISP filters."
msgstr ""
"<a href=\"https://en.wikipedia.org/wiki/"
"BitTorrent_protocol_encryption\">BitTorrent Protocol Encryption</a> can be "
"used to improve privacy. It also can help to bypass ISP filters."

#: data/gtk/preferences_dialog.ui:453
msgid "_Encryption"
msgstr "_Encryption"

#: data/gtk/preferences_dialog.ui:459
msgid "Force"
msgstr "Force"

#: data/gtk/preferences_dialog.ui:460
msgid "Prefer"
msgstr "Prefer"

#: data/gtk/preferences_dialog.ui:461
msgid "Allow"
msgstr "Allow"

#: data/gtk/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/gtk/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Show Shortcuts"

#: data/gtk/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Preferences"

#: data/gtk/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Quit"
msgstr "Quit"

#: data/gtk/shortcuts.ui:34
msgctxt "shortcut window"
msgid "Torrents"
msgstr "Torrents"

#: data/gtk/shortcuts.ui:37
msgctxt "shortcut window"
msgid "Add New Torrent"
msgstr "Add New Torrent"

#: data/gtk/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Search"
msgstr "Search"

#: data/gtk/stats_dialog.ui:6
msgid "Statistics"
msgstr "Statistics"

#: data/gtk/stats_dialog.ui:18
msgid "These statistics are from the remote connection"
msgstr "These statistics are from the remote connection"

#. Transmission is the name of the torrent daemon. Don't translate it.
#: data/gtk/stats_dialog.ui:32
msgid "Transmission Daemon"
msgstr "Transmission Daemon"

#: data/gtk/stats_dialog.ui:33
msgid "Version"
msgstr "Version"

#: data/gtk/stats_dialog.ui:49
msgid "Current Network Speed"
msgstr "Current Network Speed"

#: data/gtk/stats_dialog.ui:52 data/gtk/torrent_dialog.ui:275
msgid "Download Speed"
msgstr "Download Speed"

#: data/gtk/stats_dialog.ui:66 data/gtk/torrent_dialog.ui:358
msgid "Upload Speed"
msgstr "Upload Speed"

#: data/gtk/stats_dialog.ui:82
msgid "Torrent Count"
msgstr "Torrent Count"

#: data/gtk/stats_dialog.ui:85 data/gtk/stats_dialog.ui:211
msgid "Total"
msgstr "Total"

#: data/gtk/stats_dialog.ui:98
msgid "Active"
msgstr "Active"

#: data/gtk/stats_dialog.ui:111 src/utils.rs:124
msgid "Paused"
msgstr "Paused"

#: data/gtk/stats_dialog.ui:124 data/gtk/stats_dialog.ui:168
#: data/gtk/stats_dialog.ui:229 data/gtk/torrent_dialog.ui:298
msgid "Downloaded"
msgstr "Downloaded"

#: data/gtk/stats_dialog.ui:139
msgid "Usage"
msgstr "Usage"

#: data/gtk/stats_dialog.ui:150
msgid "This Session"
msgstr "This Session"

#: data/gtk/stats_dialog.ui:155 data/gtk/stats_dialog.ui:216
msgid "Time Active"
msgstr "Time Active"

#: data/gtk/stats_dialog.ui:181 data/gtk/stats_dialog.ui:242
#: data/gtk/torrent_dialog.ui:381
msgid "Uploaded"
msgstr "Uploaded"

#: data/gtk/stats_dialog.ui:194 data/gtk/stats_dialog.ui:255
msgid "Added Files"
msgstr "Added Files"

#: data/gtk/stats_dialog.ui:268
msgid "Started"
msgstr "Started"

#: data/gtk/torrent_dialog.ui:8
msgid "Torrent Details"
msgstr "Torrent Details"

#: data/gtk/torrent_dialog.ui:18
msgid "Overview"
msgstr "Overview"

#: data/gtk/torrent_dialog.ui:87 data/gtk/torrent_row.ui:236
msgid "Pause"
msgstr "Pause"

#: data/gtk/torrent_dialog.ui:114 data/gtk/torrent_row.ui:253
msgid "Continue"
msgstr "Continue"

#: data/gtk/torrent_dialog.ui:170
msgid "Remove"
msgstr "Remove"

#: data/gtk/torrent_dialog.ui:196
msgid "More"
msgstr "More"

#: data/gtk/torrent_dialog.ui:215
msgid "A Problem Has Occurred"
msgstr "A Problem Has Occurred"

#: data/gtk/torrent_dialog.ui:288 data/gtk/torrent_dialog.ui:371
msgid "Peers"
msgstr "Peers"

#: data/gtk/torrent_dialog.ui:401
msgid "Receiving Files Metadata…"
msgstr "Receiving Files Metadata…"

#: data/gtk/torrent_menu.ui:6
msgid "_Pause"
msgstr "_Pause"

#: data/gtk/torrent_menu.ui:11
msgid "_Continue"
msgstr "_Continue"

#: data/gtk/torrent_menu.ui:18
msgid "Queue Move _Up"
msgstr "Queue Move _Up"

#: data/gtk/torrent_menu.ui:22
msgid "Queue Move _Down"
msgstr "Queue Move _Down"

#: data/gtk/torrent_menu.ui:28 src/app.rs:436
msgid "_Open"
msgstr "_Open"

#: data/gtk/torrent_menu.ui:32 src/actions.rs:277 src/app.rs:485
msgid "_Remove"
msgstr "_Remove"

#: data/gtk/torrent_menu.ui:36
msgid "_More"
msgstr "_More"

#: data/gtk/torrent_menu.ui:38
msgid "_Copy Magnet Link"
msgstr "_Copy Magnet Link"

#: data/gtk/torrent_menu.ui:42
msgid "_Ask Tracker for More Peers"
msgstr "_Ask Tracker for More Peers"

#: data/gtk/torrent_menu.ui:46
msgid "Set _Location…"
msgstr "Set _Location…"

#: data/gtk/torrent_menu.ui:53
msgid "_Properties"
msgstr "_Properties"

#: data/gtk/torrent_page.ui:6
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Downloading"
msgstr "Downloading"

#: data/gtk/torrent_page.ui:11
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Queued"
msgstr "Queued"

#: data/gtk/torrent_page.ui:16
msgctxt "The title of a group of torrents (downloading/queued/seeding)"
msgid "Seeding"
msgstr "Seeding"

#: data/gtk/torrent_row.ui:25
msgid "Stopped"
msgstr "Stopped"

#: data/gtk/torrent_row.ui:72
msgid "Downloading"
msgstr "Downloading"

#: data/gtk/torrent_row.ui:90
msgid "Uploading"
msgstr "Uploading"

#: data/gtk/torrent_row.ui:164
msgid "Torrent progress"
msgstr "Torrent progress"

#: data/gtk/torrent_row.ui:191
msgid "Delete"
msgstr "Delete"

#: data/gtk/torrent_row.ui:212
msgid "Queue – Move up"
msgstr "Queue – Move up"

#: data/gtk/torrent_row.ui:220
msgid "Queue – Move down"
msgstr "Queue – Move down"

#: data/gtk/window.ui:20
msgid "Add New Torrent"
msgstr "Add New Torrent"

#: data/gtk/window.ui:27
msgid "Main Menu"
msgstr "Main Menu"

#: data/gtk/window.ui:35
msgid "Remote Connection Menu"
msgstr "Remote Connection Menu"

#: data/gtk/window.ui:62
msgid "Search torrents…"
msgstr "Search torrents…"

#: data/gtk/window.ui:71
msgid "Search torrents"
msgstr "Search torrents"

#: data/gtk/window.ui:87
msgid "_Resume All"
msgstr "_Resume All"

#: data/gtk/window.ui:91
msgid "_Pause All"
msgstr "_Pause All"

#: data/gtk/window.ui:95
msgid "_Remove Completed Torrents"
msgstr "_Remove Completed Torrents"

#: data/gtk/window.ui:99
msgid "_Statistics…"
msgstr "_Statistics…"

#: data/gtk/window.ui:105
msgid "_Add Remote Connection…"
msgstr "_Add Remote Connection…"

#: data/gtk/window.ui:111
msgid "_Preferences"
msgstr "_Preferences"

#: data/gtk/window.ui:115
msgid "_Keyboard Shortcuts"
msgstr "_Keyboard Shortcuts"

#: data/gtk/window.ui:119
msgid "_About Fragments"
msgstr "_About Fragments"

#: src/actions.rs:82
msgid "Unable to copy magnet link to clipboard"
msgstr "Unable to copy magnet link to clipboard"

#: src/actions.rs:86
msgid "Copied magnet link to clipboard"
msgstr "Copied magnet link to clipboard"

#: src/actions.rs:269
msgid "Remove Torrent?"
msgstr "Remove Torrent?"

#: src/actions.rs:271
msgid ""
"Once removed, continuing the transfer will require the torrent file or "
"magnet link."
msgstr ""
"Once removed, continuing the transfer will require the torrent file or "
"magnet link."

#: src/actions.rs:275 src/app.rs:483
msgid "_Cancel"
msgstr "_Cancel"

#. Check button
#: src/actions.rs:281
msgid "Remove downloaded data as well"
msgstr "Remove downloaded data as well"

#: src/actions.rs:308
msgid "Set Torrent Location"
msgstr "Set Torrent Location"

#: src/actions.rs:312 src/utils.rs:165
msgid "_Select"
msgstr "_Select"

#: src/actions.rs:321
msgid "Move Data?"
msgstr "Move Data?"

#: src/actions.rs:322
msgid ""
"In order to continue using the torrent, the data must be available at the "
"new location."
msgstr ""
"In order to continue using the torrent, the data must be available at the "
"new location."

#: src/actions.rs:325
msgid "_Only Change Location"
msgstr "_Only Change Location"

#: src/actions.rs:326
msgid "_Move Data to New Location"
msgstr "_Move Data to New Location"

#: src/actions.rs:381
msgid "Unable to open file / folder"
msgstr "Unable to open file / folder"

#: src/actions.rs:386
msgid "Could not open file"
msgstr "Could not open file"

#: src/app.rs:315
msgid "Downloading data…"
msgstr "Downloading data…"

#: src/app.rs:379
msgid "Unsupported file format"
msgstr "Unsupported file format"

#: src/app.rs:435
msgid "Open Torrents"
msgstr "Open Torrents"

#: src/app.rs:440
msgid "Torrent files"
msgstr "Torrent files"

#: src/app.rs:445
msgid "All files"
msgstr "All files"

#. NOTE: Title of the modal when the user removes all downloaded torrents
#: src/app.rs:474
msgid "Remove all Downloaded Torrents?"
msgstr "Remove all Downloaded Torrents?"

#: src/app.rs:478
msgid ""
"This will only remove torrents from Fragments, but will keep the downloaded "
"content."
msgstr ""
"This will only remove torrents from Fragments, but will keep the downloaded "
"content."

#: src/backend/connection.rs:95
msgid "Local Fragments session"
msgstr "Local Fragments session"

#: src/backend/connection_manager.rs:220 src/backend/connection_manager.rs:403
msgid "Unable to stop transmission-daemon"
msgstr "Unable to stop transmission-daemon"

#: src/backend/connection_manager.rs:229 src/backend/connection_manager.rs:360
msgid "Unable to start transmission-daemon"
msgstr "Unable to start transmission-daemon"

#: src/ui/about_dialog.rs:44
msgid "translator-credits"
msgstr ""
"Zander Brown <zbrown@gnome.org>\n"
"Andi Chandler <andi@gowling.com>\n"
"Bruce Cowan <bruce@bcowan.me.uk>"

#: src/ui/add_connection_dialog.rs:112
msgid ""
"Could not connect with “{}”:\n"
"{}"
msgstr ""
"Could not connect with “{}”:\n"
"{}"

#: src/ui/connection_box.rs:105
msgid "The configured download directory cannot be accessed."
msgstr "The configured download directory cannot be accessed."

#: src/ui/connection_box.rs:108
msgid "The configured incomplete directory cannot be accessed."
msgstr "The configured incomplete directory cannot be accessed."

#: src/ui/connection_box.rs:155
msgid "Connection with {} was lost."
msgstr "Connection with {} was lost."

#: src/ui/connection_box.rs:275
msgid "Credentials could not be saved"
msgstr "Credentials could not be saved"

#: src/ui/connection_box.rs:315
msgid "No connection found with UUID \"{}\""
msgstr "No connection found with UUID \"{}\""

#: src/ui/connection_box.rs:360
msgid "Failed to establish a connection with the Transmission service (“{}”)."
msgstr "Failed to establish a connection with the Transmission service (“{}”)."

#: src/ui/connection_row.rs:85
msgid "This computer"
msgstr "This computer"

#: src/ui/file_row.rs:199
msgid "{} / {}, {} item"
msgid_plural "{} / {}, {} items"
msgstr[0] "{} / {}, {} item"
msgstr[1] "{} / {}, {} items"

#: src/ui/file_row.rs:201
msgid "{} / {}"
msgstr "{} / {}"

#: src/ui/file_row.rs:204
msgid "{}, {} item"
msgid_plural "{}, {} items"
msgstr[0] "{}, {} item"
msgstr[1] "{}, {} items"

#: src/ui/preferences_dialog.rs:164
msgid "Unable to restart Transmission daemon"
msgstr "Unable to restart Transmission daemon"

#: src/ui/preferences_dialog.rs:323
msgid "Testing…"
msgstr "Testing…"

#: src/ui/preferences_dialog.rs:334
msgid "Port is open. You can communicate with other peers."
msgstr "Port is open. You can communicate with other peers."

#: src/ui/preferences_dialog.rs:337
msgid ""
"Port is closed. Communication with other peers is limited.\n"
"Check your router or firewall if port forwarding is enabled for your "
"computer. "
msgstr ""
"Port is closed. Communication with other peers is limited.\n"
"Check your router or firewall if port forwarding is enabled for your "
"computer. "

#: src/ui/preferences_dialog.rs:342
msgid "Test failed. Make sure you are connected to the internet."
msgstr "Test failed. Make sure you are connected to the Internet."

#: src/ui/preferences_dialog.rs:373 src/ui/stats_dialog.rs:268
msgid "You are connected with “{}”"
msgstr "You are connected with “{}”"

#: src/ui/stats_dialog.rs:252
msgid "{} time"
msgid_plural "{} times"
msgstr[0] "{} time"
msgstr[1] "{} times"

#: src/ui/torrent_dialog.rs:341
msgid "Show All"
msgid_plural "Show All {} Items"
msgstr[0] "Show All"
msgstr[1] "Show All {} Items"

#: src/ui/torrent_dialog.rs:395
msgid "{} ({} active)"
msgid_plural "{} ({} active)"
msgstr[0] "{} ({} active)"
msgstr[1] "{} ({} active)"

#. Translators: First {} is the amount uploaded, second {} is the
#. uploading speed
#. Displayed under the torrent name in the main window when torrent
#. download is finished
#: src/ui/torrent_row.rs:247
msgid "{} uploaded · {}"
msgstr "{} uploaded · {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size
#. Displayed under the torrent name in the main window when torrent
#. is stopped
#: src/ui/torrent_row.rs:255
msgid "{} of {}"
msgstr "{} of {}"

#. Translators: First {} is the amount downloaded, second {} is the
#. total torrent size, third {} is the download speed
#. Displayed under the torrent name in the main window when torrent
#. is downloading
#: src/ui/torrent_row.rs:261
msgid "{} of {} · {}"
msgstr "{} of {} · {}"

#: src/ui/window.rs:107
msgid "Remote control \"{}\""
msgstr "Remote control \"{}\""

#: src/ui/window.rs:176
msgid "New torrent added"
msgstr "New torrent added"

#: src/ui/window.rs:185
msgid "Torrent completely downloaded"
msgstr "Torrent completely downloaded"

#: src/ui/window.rs:248
msgid "Add magnet link “{}” from clipboard?"
msgstr "Add magnet link “{}” from clipboard?"

#: src/ui/window.rs:249
msgid "Add magnet link from clipboard?"
msgstr "Add magnet link from clipboard?"

#: src/ui/window.rs:254 src/ui/window.rs:275
msgid "_Add"
msgstr "_Add"

#: src/ui/window.rs:263
msgid "Add torrent from “{}”?"
msgstr "Add torrent from “{}”?"

#: src/ui/window.rs:270
#| msgid "Add magnet link “{}” from clipboard?"
msgid "Add “{}” from clipboard?"
msgstr "Add “{}” from clipboard?"

#: src/utils.rs:96
msgid "more than a day"
msgstr "more than a day"

#: src/utils.rs:104
msgid "{} day"
msgid_plural "{} days"
msgstr[0] "{} day"
msgstr[1] "{} days"

#: src/utils.rs:105
msgid "{} hour"
msgid_plural "{} hours"
msgstr[0] "{} hour"
msgstr[1] "{} hours"

#: src/utils.rs:106
msgid "{} minute"
msgid_plural "{} minutes"
msgstr[0] "{} minute"
msgstr[1] "{} minutes"

#: src/utils.rs:107
msgid "{} second"
msgid_plural "{} seconds"
msgstr[0] "{} second"
msgstr[1] "{} seconds"

#. i18n: "Queued" is the current status of a torrent
#: src/utils.rs:130 src/utils.rs:133 src/utils.rs:136
msgid "Queued"
msgstr "Queued"

#: src/utils.rs:131
msgid "Checking…"
msgstr "Checking…"

#: src/utils.rs:134
msgid "Seeding…"
msgstr "Seeding…"

#: src/utils.rs:158
msgid "Select Download Directory"
msgstr "Select Download Directory"

#: src/utils.rs:159
msgid "Select Incomplete Directory"
msgstr "Select Incomplete Directory"

#~ msgid "A BitTorrent Client"
#~ msgstr "A BitTorrent Client"

#~ msgid "A BitTorrent Client – Based on Transmission Technology"
#~ msgstr "A BitTorrent Client – Based on Transmission Technology"

#~ msgid "Fragments"
#~ msgstr "Fragments"

#~ msgid "Felix Häcker"
#~ msgstr "Felix Häcker"

#~ msgid "Appearance"
#~ msgstr "Appearance"

#~ msgid "Slim Mode"
#~ msgstr "Slim Mode"

#~ msgid "Make everything a bit smaller to display more information"
#~ msgstr "Make everything a bit smaller to display more information"

#~ msgid "Dark Mode"
#~ msgstr "Dark Mode"

#~ msgid "Whether Fragments should use a dark theme"
#~ msgstr "Whether Fragments should use a dark theme"

#~ msgid "Prefer encryption"
#~ msgstr "Prefer encryption"

#~ msgid "Force encryption"
#~ msgstr "Force encryption"

#~ msgid "Please wait…"
#~ msgstr "Please wait…"

#~ msgid "Seeders"
#~ msgstr "Seeders"

#~ msgid "Speed"
#~ msgstr "Speed"

#~ msgid "Leechers"
#~ msgstr "Leechers"

#~ msgid "Request more peers"
#~ msgstr "Request more peers"

#~ msgid "Turbo Boost"
#~ msgstr "Turbo Boost"

#~ msgid "Speeds up this torrent at the expense of others"
#~ msgstr "Speeds up this torrent at the expense of others"

#~ msgid "Add"
#~ msgstr "Add"

#~ msgid "GitLab Homepage"
#~ msgstr "GitLab Homepage"

#~ msgid "Select download folder"
#~ msgstr "Select download folder"

#, c-format
#~ msgid "%s of %s"
#~ msgstr "%s of %s"

#, c-format
#~ msgid "%s of %s · %s"
#~ msgstr "%s of %s · %s"

#~ msgid "kB"
#~ msgstr "kB"

#~ msgid "MB"
#~ msgstr "MB"

#~ msgid "GB"
#~ msgstr "GB"

#~ msgid "TB"
#~ msgstr "TB"

#~ msgid "kB/s"
#~ msgstr "kB/s"

#~ msgid "MB/s"
#~ msgstr "MB/s"

#~ msgid "GB/s"
#~ msgstr "GB/s"

#~ msgid "TB/s"
#~ msgstr "TB/s"
