// Fragments - mod.rs
// Copyright (C) 2022-2024  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod about_dialog;
mod add_connection_dialog;
mod connection_box;
mod connection_popover;
mod connection_row;
mod drag_overlay;
mod file_page;
mod file_row;
mod folder_page;
mod folder_page_contents;
mod preferences_dialog;
mod stats_dialog;
mod status_header;
mod torrent_dialog;
mod torrent_group;
mod torrent_page;
mod torrent_row;
mod window;

pub use add_connection_dialog::FrgAddConnectionDialog;
pub use connection_box::FrgConnectionBox;
pub use connection_popover::FrgConnectionPopover;
pub use connection_row::FrgConnectionRow;
pub use drag_overlay::FrgDragOverlay;
pub use file_page::FrgFilePage;
pub use file_row::FrgFileRow;
pub use folder_page::FrgFolderPage;
pub use folder_page_contents::FrgFolderPageContents;
pub use preferences_dialog::FrgPreferencesDialog;
pub use stats_dialog::FrgStatsDialog;
pub use status_header::FrgStatusHeader;
pub use torrent_dialog::FrgTorrentDialog;
pub use torrent_group::FrgTorrentGroup;
pub use torrent_page::FrgTorrentPage;
pub use torrent_row::FrgTorrentRow;
pub use window::FrgApplicationWindow;
